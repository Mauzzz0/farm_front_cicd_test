import React from 'react';
import { colors } from '../../theme/colors';

interface IAddIcon {
  color?: string;
}

const AddIcon: React.FC<IAddIcon> = ({ color }) => {
  return (
    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M19 13H13V19H11V13H5V11H11V5H13V11H19V13Z" fill={color || colors.dark_grey} />
    </svg>
  );
};

export default AddIcon;
