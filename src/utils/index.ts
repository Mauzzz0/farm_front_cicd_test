import { roleInfo, statusInfo } from '../models/EmployeeModel';
import { EmployeeResponse, StatusEnum } from '../models/Employee';
import { ClientResponse } from '../models/ClientModel';
import dayjs from 'dayjs';
import { callStatusInfo } from '../models/Call';

export const validateAuthInput = (
  targetValue: string,
  initialValue: string,
  maxLength: number,
  setValue: (s: string) => void,
  setIsDisabled: (b: boolean) => void,
) => {
  const typedSymbol = targetValue.slice(-1);
  const typedSymbolIsDigit = isDigit(typedSymbol);

  if (targetValue.length <= 0) {
    setValue(initialValue);
  } else if (targetValue.length <= maxLength && typedSymbolIsDigit) {
    setValue(targetValue);
    setIsDisabled(targetValue.length !== maxLength);
  }
};

export const validateNumberInput = (targetValue: string, maxLength: number) => {
  const typedSymbol = targetValue.slice(-1);
  const typedSymbolIsDigit = isDigit(typedSymbol);

  if (targetValue.length <= maxLength && typedSymbolIsDigit) {
    return targetValue;
  } else if (targetValue.length > maxLength) {
    return targetValue.split('', maxLength).join('');
  }
  return targetValue.split('', targetValue.length - 1).join('');
};

const isDigit = (digit: string): boolean => {
  if (digit.trim() === '') {
    return false;
  }

  return [...Array(10).keys()].indexOf(Number(digit)) !== -1;
};

export const getCurrentRoleName = (user: EmployeeResponse) => {
  const role = roleInfo.options.find((role) => role.name === user.role);
  return role?.title ?? '';
};

export const getCurrentStatusName = (user: EmployeeResponse | ClientResponse) => {
  const statusName: StatusEnum = user.blocked ? StatusEnum.Blocked : StatusEnum.Active;
  const status = statusInfo.options.find((status) => status.name === statusName);
  return status?.title ?? '';
};

export const getSelectedRoleName = (value: string) => {
  const role = roleInfo.options.find((role) => role.name === value);
  return role?.title ?? '';
};

export const getSelectedStatusName = (value: string) => {
  const status = statusInfo.options.find((status) => status.name === value);
  return status?.title ?? '';
};

export const getCallStatusName = (value: string) => {
  const status = callStatusInfo.options.find((status) => status.name === value);
  return status?.title ?? '';
};

export const roleIsNotUser = (role: string) => {
  return role === 'Courier' || role === 'Admin' || role === 'SAdmin';
};

export const formatTimeDateOrEmptyString = (datetime: string): string => {
  if (dayjs(datetime).isValid()) {
    return dayjs(datetime).format('H:mm   DD.MM.YYYY');
  }

  return '';
};
