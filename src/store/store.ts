import { combineReducers, configureStore } from '@reduxjs/toolkit';
import authReducer from './reducers/auth/AuthSlice';
import employeeReducer from './reducers/employee/UserSlice';
import crumbReducer from './reducers/breadcrumb/CrumbSlice';
import clientReducer from './reducers/client/ClientSlice';
import callReducer from './reducers/call/CallSlice';
import pharmReducer from './reducers/pharmacy/PharmSlice';
import logoReducer from './reducers/logo/LogoSlice';

const rootReducer = combineReducers({
  authReducer,
  employeeReducer,
  crumbReducer,
  clientReducer,
  callReducer,
  pharmReducer,
  logoReducer,
});

export const setupStore = () => {
  return configureStore({
    reducer: rootReducer,
    middleware: (getDefaultMiddleware) =>
      getDefaultMiddleware({
        serializableCheck: false,
      }),
  });
};

export type RootState = ReturnType<typeof rootReducer>;
export type AppStore = ReturnType<typeof setupStore>;
export type AppDispatch = AppStore['dispatch'];
