import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export interface IBreadcrumbs {
  name: string;
  path: string;
}
interface IHeading {
  title: string;
  breadcrumbs: IBreadcrumbs[];
}

const initialState: IHeading = {
  title: '',
  breadcrumbs: [],
};

export const crumbSlice = createSlice({
  name: 'breadcrumbs',
  initialState,
  reducers: {
    setTitle(state, action: PayloadAction<string>) {
      state.title = action.payload;
    },
    setBreadcrumbs(state, action: PayloadAction<IBreadcrumbs[]>) {
      state.breadcrumbs = action.payload;
    },
  },
});

export const { setTitle, setBreadcrumbs } = crumbSlice.actions;
export default crumbSlice.reducer;
