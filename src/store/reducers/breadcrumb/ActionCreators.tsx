import { AppDispatch } from '../../store';
import { IBreadcrumbs, setBreadcrumbs, setTitle } from './CrumbSlice';
import { mainRoutes } from '../../../routes';

export const setLocation = (currentPath: string, title: string) => (dispatch: AppDispatch) => {
  let breadcrumbs: IBreadcrumbs[] = [];

  const arrFromPath = currentPath.split('/');

  arrFromPath.reduce((prev, current, idx) => {
    let pathName = title;
    if (idx === 1) {
      const mainPath = mainRoutes.find((item) => item.path.includes(current));
      pathName = mainPath?.name ?? '';
    }
    prev += `/${current}`;
    breadcrumbs = [
      ...breadcrumbs,
      {
        name: pathName,
        path: prev,
      },
    ];

    dispatch(setTitle(pathName));
    return current;
  });
  dispatch(setBreadcrumbs(breadcrumbs));
};
