import { CallResponse } from '../../../models/Call';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';

interface ICallSlice {
  callList: (string | JSX.Element)[][];
  call: CallResponse;
  isLoading: boolean;
  error: string;
}

const initialState: ICallSlice = {
  callList: [],
  call: {
    id: '',
    name: '',
    phone: '',
    created: '',
    doneAt: '',
    userComment: '',
    adminComment: '',
    done: false,
  },
  isLoading: false,
  error: '',
};

export const callSlice = createSlice({
  name: 'call',
  initialState,
  reducers: {
    setCallList(state, action: PayloadAction<(string | JSX.Element)[][]>) {
      state.callList = action.payload;
    },
    setCall(state, action: PayloadAction<CallResponse>) {
      state.call = action.payload;
    },
    setLoading(state, action: PayloadAction<boolean>) {
      state.isLoading = action.payload;
    },
    setError(state, action: PayloadAction<string>) {
      state.error = action.payload;
    },
  },
});

export const { setCallList, setCall, setError, setLoading } = callSlice.actions;

export default callSlice.reducer;
