import { AppDispatch } from '../../store';
import CallService from '../../../services/CallService';
import {
  CallResponse,
  callResponseModel,
  CallStatusEnum,
  callStatusInfo,
} from '../../../models/Call';
import { setCall, setCallList, setError, setLoading } from './CallSlice';
import ColorFlags from '../../../components/ui/colorFlags/colorFlags';
import { formatTimeDateOrEmptyString } from '../../../utils';

export const fetchCalls = (query?: string, done?: boolean) => async (dispatch: AppDispatch) => {
  try {
    const response = await CallService.fetchCalls(query, done);
    const callList: (string | JSX.Element)[][] = response.data.payload.items.reduce(
      (prev, current) => {
        const statusName = current.done ? CallStatusEnum.Done : CallStatusEnum.Waiting;
        const statusInfo = callStatusInfo.options.find((item) => item.name === statusName);

        const modelData: (string | JSX.Element)[] = callResponseModel.map((key) => {
          return key.name === 'done' ? (
            <ColorFlags
              bg={statusInfo?.bg ?? ''}
              width={callStatusInfo.width}
              title={statusInfo?.title ?? ''}
              icon={statusInfo?.icon ?? ''}
            />
          ) : key.name === 'created' || key.name === 'doneAt' ? (
            formatTimeDateOrEmptyString(current[key.name])
          ) : (
            current[key.name]
          );
        });
        return [...prev, modelData];
      },
      [] as any[],
    );

    dispatch(setCallList(callList));
  } catch (e) {
    if (e instanceof Error) {
      dispatch(setError(e.message));
    } else {
      console.log('Unexpected error', e);
    }
  }
};

export const getCallById = (id: string) => async (dispatch: AppDispatch) => {
  try {
    dispatch(setLoading(true));
    const response = await CallService.getCallById(id);
    dispatch(setCall(response.data.payload));
    dispatch(setLoading(false));
  } catch (e) {
    if (e instanceof Error) {
      dispatch(setError(e.message));
    } else {
      console.log('Unexpected error', e);
    }
  }
};

export const updateCall = (data: CallResponse) => async (dispatch: AppDispatch) => {
  try {
    dispatch(setLoading(true));
    await CallService.updateCall(data.id, { done: data.done, adminComment: data.adminComment });
    dispatch(setLoading(false));
  } catch (e) {
    if (e instanceof Error) {
      dispatch(setError(e.message));
    } else {
      console.log('Unexpected error', e);
    }
  }
};
