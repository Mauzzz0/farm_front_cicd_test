import { PharmResponse } from '../../../models/Pharmacy';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';

interface IPharmSlice {
  pharmList: (string | JSX.Element)[][];
  pharm: PharmResponse;
  isLoading: boolean;
  error: string;
}

const initialState: IPharmSlice = {
  pharmList: [],
  pharm: {
    id: '',
    name: '',
    phone: '',
    works: '',
    map: '',
    lat: 0,
    lon: 0,
    icon: [],
  },
  isLoading: false,
  error: '',
};

export const pharmSlice = createSlice({
  name: 'pharm',
  initialState,
  reducers: {
    setPharmList(state, action: PayloadAction<(string | JSX.Element)[][]>) {
      state.pharmList = action.payload;
    },
    setPharm(state, action: PayloadAction<PharmResponse>) {
      state.pharm = action.payload;
    },
    setLoading(state, action: PayloadAction<boolean>) {
      state.isLoading = action.payload;
    },
    setError(state, action: PayloadAction<string>) {
      state.error = action.payload;
    },
  },
});

export const { setPharmList, setPharm, setLoading, setError } = pharmSlice.actions;

export default pharmSlice.reducer;
