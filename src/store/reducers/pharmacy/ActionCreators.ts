import { AppDispatch } from '../../store';
import { setLoading, setPharm, setPharmList } from './PharmSlice';
import { PharmService } from '../../../services/PharmService';
import { PharmResponse, pharmResponseModel } from '../../../models/Pharmacy';
import { setError } from '../call/CallSlice';

export const fetchPharms = () => async (dispatch: AppDispatch) => {
  try {
    dispatch(setLoading(true));
    const response = await PharmService.fetchPharms();
    const pharmList: (string | JSX.Element)[][] = response.data.payload.items.reduce(
      (prev, current) => {
        const modelData = pharmResponseModel.map((key) => {
          return current[key.name];
        });
        return [...prev, modelData];
      },
      [] as any[],
    );
    dispatch(setPharmList(pharmList));
    dispatch(setLoading(false));
  } catch (e) {
    if (e instanceof Error) {
      dispatch(setError(e.message));
    } else {
      console.log('Unexpected error', e);
    }
  }
};

export const getPharmById = (id: string) => async (dispatch: AppDispatch) => {
  try {
    dispatch(setLoading(true));
    const response = await PharmService.getPharmById(id);
    dispatch(setPharm(response.data.payload));
    dispatch(setLoading(false));
  } catch (e) {
    if (e instanceof Error) {
      dispatch(setError(e.message));
    } else {
      console.log('Unexpected error', e);
    }
  }
};

export const updatePharm = (data: PharmResponse) => async (dispatch: AppDispatch) => {
  try {
    const { id, ...others } = { ...data };
    dispatch(setLoading(true));
    await PharmService.updatePharm(id, others);
    dispatch(setLoading(false));
  } catch (e) {
    if (e instanceof Error) {
      dispatch(setError(e.message));
    } else {
      console.log('Unexpected error', e);
    }
  }
};

export const postPharm = (data: PharmResponse) => async (dispatch: AppDispatch) => {
  try {
    dispatch(setLoading(true));
    await PharmService.postPharm(data);
    dispatch(setLoading(false));
  } catch (e) {
    if (e instanceof Error) {
      dispatch(setError(e.message));
    } else {
      console.log('Unexpected error', e);
    }
  }
};

export const deletePharm = (id: string) => async (dispatch: AppDispatch) => {
  try {
    dispatch(setLoading(true));
    await PharmService.deletePharm(id);
    dispatch(setLoading(false));
  } catch (e) {
    if (e instanceof Error) {
      dispatch(setError(e.message));
    } else {
      console.log('Unexpected error', e);
    }
  }
};
