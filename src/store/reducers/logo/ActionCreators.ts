import { AppDispatch } from '../../store';
import { setError } from '../call/CallSlice';
import { setLoading, setLogo, setLogoList } from './LogoSlice';
import { LogoService } from '../../../services/LogoService';
import * as _ from 'lodash';
import { LogoInitResponse, LogoUpdate } from '../../../models/Logo';

export const fetchLogos = () => async (dispatch: AppDispatch) => {
  try {
    dispatch(setLoading(true));

    const [logoResponse, categoryResponse] = await Promise.all([
      LogoService.fetchLogos(),
      LogoService.fetchCategories(),
    ]);

    const logos = logoResponse.data.payload.items;
    const categories = categoryResponse.data.payload.list;

    const result = _.map(logos, (obj) => {
      return _.assign(obj, _.find(categories, { groupId: obj.id }));
    });

    dispatch(setLogoList(result));
    dispatch(setLoading(false));
  } catch (e) {
    if (e instanceof Error) {
      dispatch(setError(e.message));
    } else {
      console.log('Unexpected error', e);
    }
  }
};

export const getLogoById = (id: string) => async (dispatch: AppDispatch) => {
  try {
    dispatch(setLoading(true));

    const [logoResponse, categoriesResponse] = await Promise.all([
      LogoService.getLogoById(id),
      LogoService.fetchCategories(),
    ]);

    const logo = logoResponse.data.payload;
    const category = categoriesResponse.data.payload.list.find((item) => item.groupId === logo.id);

    dispatch(setLogo({ ...logo, groupName: category?.groupName ?? '' }));
    dispatch(setLoading(false));
  } catch (e) {
    if (e instanceof Error) {
      dispatch(setError(e.message));
    } else {
      console.log('Unexpected error', e);
    }
  }
};

export const updateLogo = (id: number, data: LogoUpdate) => async (dispatch: AppDispatch) => {
  try {
    dispatch(setLoading(true));
    await LogoService.updateLogo(id, data);
    dispatch(setLoading(false));
  } catch (e) {
    if (e instanceof Error) {
      dispatch(setError(e.message));
    } else {
      console.log('Unexpected error', e);
    }
  }
};

export const deleteLogo = (id: number) => async (dispatch: AppDispatch) => {
  try {
    dispatch(setLoading(true));
    await LogoService.deleteLogo(id);
    dispatch(setLoading(false));
  } catch (e) {
    if (e instanceof Error) {
      dispatch(setError(e.message));
    } else {
      console.log('Unexpected error', e);
    }
  }
};

export const postLogo = (data: LogoInitResponse) => async (dispatch: AppDispatch) => {
  try {
    dispatch(setLoading(true));
    await LogoService.postLogo(data);
    dispatch(setLoading(false));
  } catch (e) {
    if (e instanceof Error) {
      dispatch(setError(e.message));
    } else {
      console.log('Unexpected error', e);
    }
  }
};
