import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { LogoResponse } from '../../../models/Logo';

interface ILogoSlice {
  logoList: LogoResponse[];
  logo: LogoResponse;
  isLoading: boolean;
  error: string;
}

const initialState: ILogoSlice = {
  logoList: [],
  logo: { id: 0, groupName: '', url: '' },
  isLoading: false,
  error: '',
};

export const LogoSlice = createSlice({
  name: 'logo',
  initialState,
  reducers: {
    setLogoList(state, action: PayloadAction<LogoResponse[]>) {
      state.logoList = action.payload;
    },
    setLogo(state, action: PayloadAction<LogoResponse>) {
      state.logo = action.payload;
    },
    setLoading(state, action: PayloadAction<boolean>) {
      state.isLoading = action.payload;
    },
    setError(state, action: PayloadAction<string>) {
      state.error = action.payload;
    },
  },
});

export const { setLogoList, setLogo, setLoading, setError } = LogoSlice.actions;

export default LogoSlice.reducer;
