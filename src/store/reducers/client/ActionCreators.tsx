import { AppDispatch } from '../../store';
import ClientService from '../../../services/ClientService';
import { setClient, setClientList, setLoading } from './ClientSlice';
import { setError } from '../employee/UserSlice';
import ColorFlags from '../../../components/ui/colorFlags/colorFlags';
import { ClientResponse, UpdateClient } from '../../../models/ClientModel';
import { ClientAddressUpdate } from '../../../models/ClientAddress';
import { statusInfo } from '../../../models/EmployeeModel';
import { StatusEnum } from '../../../models/Employee';

export const fetchClients = (query?: string, blocked?: boolean) => {
  return async (dispatch: AppDispatch) => {
    try {
      const response = await ClientService.getClients(query, blocked);
      const clientsItems = response.data.payload.items;

      const newClientList = clientsItems.map((item) => {
        const { blocked, ...other } = item;
        const statusName = blocked ? StatusEnum.Blocked : StatusEnum.Active;
        const statusParams = statusInfo.options.find((item) => item.name === statusName);

        return {
          ...other,
          blocked: (
            <ColorFlags
              bg={statusParams?.bg ?? ''}
              title={statusParams?.title ?? ''}
              icon={statusParams?.icon ?? ''}
              width={statusInfo.width}
            />
          ),
        };
      });

      dispatch(setClientList(newClientList));
    } catch (e) {
      if (e instanceof Error) {
        dispatch(setError(e.message));
      } else {
        console.log('Unexpected error', e);
      }
    }
  };
};

export const getClientById = (id: string) => async (dispatch: AppDispatch) => {
  try {
    dispatch(setLoading(true));
    const response = await ClientService.getClientById(id);
    dispatch(setClient({ ...response.data.payload }));
    dispatch(setLoading(false));
  } catch (e) {
    if (e instanceof Error) {
      dispatch(setError(e.message));
    } else {
      console.log('Unexpected error', e);
    }
  }
};

export const updateClient = (data: ClientResponse) => async (dispatch: AppDispatch) => {
  try {
    const { id } = { ...data };
    const newData: UpdateClient = {
      name: data.name,
      surname: data.surname,
      patronymic: data.patronymic,
      blocked: data.blocked,
    };

    return await ClientService.updateClient(id, newData);
  } catch (e) {
    if (e instanceof Error) {
      dispatch(setError(e.message));
    } else {
      console.log('Unexpected error', e);
    }
  }
};

export const updateClientAddress =
  (userId: string, addressId: string, data: ClientAddressUpdate) =>
  async (dispatch: AppDispatch) => {
    try {
      const newData: ClientAddressUpdate = {
        region: data.region,
        city: data.city,
        street: data.street,
        house: data.house,
        entrance: data.entrance,
        floor: data.floor,
        apartments: data.apartments,
        comment: data.comment,
      };

      return await ClientService.updateClientAddress(userId, addressId, newData);
    } catch (e) {
      if (e instanceof Error) {
        dispatch(setError(e.message));
      } else {
        console.log('Unexpected error', e);
      }
    }
  };
