import { ClientResponse } from '../../../models/ClientModel';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { ClientAddressResponse } from '../../../models/ClientAddress';

export interface IClient {
  id: string;
  name: string;
  surname: string;
  patronymic: string;
  phone: string;
  blocked: JSX.Element;
  addresses: ClientAddressResponse[];
}

interface IClientSlice {
  clientList: IClient[];
  client: ClientResponse;
  isLoading: boolean;
  error: string;
}

const initialState: IClientSlice = {
  clientList: [],
  client: {
    id: '',
    name: '',
    surname: '',
    patronymic: '',
    phone: '',
    blocked: true,
    addresses: [],
  },
  isLoading: false,
  error: '',
};

export const clientSlice = createSlice({
  name: 'client',
  initialState,
  reducers: {
    setClientList(state, action: PayloadAction<IClient[]>) {
      state.clientList = action.payload;
    },
    setClient(state, action: PayloadAction<ClientResponse>) {
      state.client = action.payload;
    },
    setLoading(state, action: PayloadAction<boolean>) {
      state.isLoading = action.payload;
    },
    setError(state, action: PayloadAction<string>) {
      state.error = action.payload;
    },
  },
});

export const { setClientList, setLoading, setError, setClient } = clientSlice.actions;

export default clientSlice.reducer;
