import { AppDispatch } from '../../store';
import EmployeeService from '../../../services/EmployeeService';
import { IEmployee, setEmployee, setEmployeeList, setError, setLoading } from './UserSlice';
import {
  EmployeeRequest,
  EmployeeResponse,
  RoleEnum,
  StatusEnum,
  UpdateEmployee,
} from '../../../models/Employee';
import ColorFlags from '../../../components/ui/colorFlags/colorFlags';
import { roleInfo, statusInfo } from '../../../models/EmployeeModel';

export const fetchEmployees = (roles: RoleEnum[], query?: string, blocked?: boolean) => {
  return async (dispatch: AppDispatch) => {
    try {
      const reqRoles = roles.map((role) => `role=${role}`).join('&');
      const response = await EmployeeService.getUsers(reqRoles, query, blocked);
      const employeeItems = response.data.payload.items;

      const newUserList = employeeItems.reduce((prev: IEmployee[], current): IEmployee[] => {
        const { blocked, role, ...other } = current;
        const statusName = blocked ? StatusEnum.Blocked : StatusEnum.Active;
        const statusParams = statusInfo.options.find((item) => item.name === statusName);
        const roleParams = roleInfo.options.find((item) => item.name === role);
        return [
          ...prev,
          {
            ...other,
            blocked: (
              <ColorFlags
                bg={statusParams?.bg ?? ''}
                title={statusParams?.title ?? ''}
                icon={statusParams?.icon ?? ''}
                width={statusInfo.width}
              />
            ),
            role: (
              <ColorFlags
                bg={roleParams?.bg ?? ''}
                title={roleParams?.title ?? ''}
                icon={roleParams?.icon ?? ''}
                width={roleInfo.width}
              />
            ),
          },
        ];
        return prev;
      }, []);

      dispatch(setEmployeeList(newUserList));
    } catch (e) {
      if (e instanceof Error) {
        dispatch(setError(e.message));
      } else {
        console.log('Unexpected error', e);
      }
    }
  };
};

export const addEmployee = (data: EmployeeRequest) => async (dispatch: AppDispatch) => {
  try {
    await EmployeeService.postUser(data);
  } catch (e) {
    if (e instanceof Error) {
      dispatch(setError(e.message));
    } else {
      console.log('Unexpected error', e);
    }
  }
};

export const getEmployeeByID = (id: string) => async (dispatch: AppDispatch) => {
  try {
    dispatch(setLoading(true));
    const response = await EmployeeService.getUserById(id);
    dispatch(setEmployee({ ...response.data.payload }));
    dispatch(setLoading(false));
  } catch (e) {
    if (e instanceof Error) {
      dispatch(setError(e.message));
    } else {
      console.log('Unexpected error', e);
    }
  }
};

export const updateEmployee = (data: EmployeeResponse) => async (dispatch: AppDispatch) => {
  try {
    const id = data.id;
    const newData: UpdateEmployee = {
      name: data.name,
      surname: data.surname,
      patronymic: data.patronymic,
      role: data.role,
      blocked: data.blocked,
    };

    return await EmployeeService.updateUser(id, newData);
  } catch (e) {
    if (e instanceof Error) {
      dispatch(setError(e.message));
    } else {
      console.log('Unexpected error', e);
    }
  }
};
