import React, { useEffect, useState } from 'react';
import PageWrapper from '../../components/ui/pageWrapper/PageWrapper';
import Heading from '../../components/heading/Heading';
import { ContentWrapper } from '../../components/ui/pageWrapper/ContentWrapper';
import { TableInputs } from '../../components/ui/table/TableInputs';
import { SimpleInput } from '../../components/ui/inputs/SimpleInput';
import { useLocation } from 'react-router-dom';
import { useAppDispatch, useAppSelector } from '../../hooks/redux';
import { setLocation } from '../../store/reducers/breadcrumb/ActionCreators';
import { fetchClients } from '../../store/reducers/client/ActionCreators';
import { clientResponseModel } from '../../models/ClientModel';
import Table from '../../components/ui/table/Table';
import TextField from '../../components/ui/inputs/TextField';
import Select from '../../components/ui/inputs/Select';
import { statusInfo } from '../../models/EmployeeModel';
import { StatusEnum } from '../../models/Employee';
import { getSelectedStatusName } from '../../utils';

const ClientsPage: React.FC = () => {
  const { pathname } = useLocation();
  const dispatch = useAppDispatch();
  const { clientList } = useAppSelector((state) => state.clientReducer);
  const [clientValues, setClientValues] = useState<(string | JSX.Element)[][]>([]);
  const [query, setQuery] = useState<string>();
  const [blocked, setBlocked] = useState<{ name: boolean | undefined; title: string }>();

  useEffect(() => {
    dispatch(fetchClients(query, blocked?.name));
    dispatch(setLocation(pathname, ''));
  }, [query, blocked]);

  useEffect(() => {
    if (clientList.length !== 0) {
      getValuesForTable();
    }
  }, [clientList]);

  const getValuesForTable = () => {
    setClientValues([]);

    const newClientValues = clientList.reduce((prev, cur) => {
      const modelData = clientResponseModel.map((key) => {
        return cur[key.name];
      });

      return [...prev, modelData];
    }, [] as any[]);

    setClientValues(newClientValues);
  };

  const selectHandler = (value: string) => {
    if (value === StatusEnum.AllStatuses) {
      setBlocked({ name: undefined, title: '' });
    } else if (value in StatusEnum) {
      setBlocked({
        name: value === StatusEnum.Blocked,
        title: getSelectedStatusName(value),
      });
    }
  };

  return (
    <PageWrapper>
      <Heading />
      <ContentWrapper flex_dir={'column'} flex_wrap={'nowrap'} gap={32}>
        <TableInputs>
          <TextField>
            <SimpleInput
              placeholder={'Поиск'}
              value={query}
              onChange={(e) => setQuery(e.target.value)}
            />
          </TextField>
          <TextField label={statusInfo.title}>
            <Select
              placeholder={'Все'}
              options={statusInfo.options}
              addOptions={[{ name: StatusEnum.AllStatuses, title: 'Все' }]}
              value={blocked?.title ?? ''}
              eventHandler={selectHandler}
            />
          </TextField>
        </TableInputs>
        {clientList.length ? (
          <Table header={clientResponseModel} data={clientValues} />
        ) : (
          <span>Пока нет клиентов</span>
        )}
      </ContentWrapper>
    </PageWrapper>
  );
};

export default ClientsPage;
