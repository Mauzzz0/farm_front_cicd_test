import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { useAppDispatch } from '../../hooks/redux';
import PageWrapper from '../../components/ui/pageWrapper/PageWrapper';
import Heading from '../../components/heading/Heading';
import { PrimaryBtn } from '../../components/ui/buttons/PrimaryBtn';
import { ContentWrapper } from '../../components/ui/pageWrapper/ContentWrapper';
import ClientFields from '../../components/client/ClientFields';
import ClientAddresses from '../../components/client/ClientAddresses';
import { getClientById } from '../../store/reducers/client/ActionCreators';

const ClientByIdPage: React.FC = () => {
  const { id } = useParams<{ id: string }>();
  const dispatch = useAppDispatch();
  const [isDisable, setDisable] = useState(true);
  const [saveClicked, setSaveClicked] = useState(false);

  useEffect(() => {
    if (id) {
      dispatch(getClientById(id));
    }
  }, [saveClicked]);

  return (
    <PageWrapper>
      <Heading>
        <PrimaryBtn size={'medium'} disabled={isDisable} onClick={() => setSaveClicked(true)}>
          Сохранить
        </PrimaryBtn>
      </Heading>
      <ContentWrapper flex_dir={'column'} flex_wrap={'nowrap'} gap={80}>
        <ClientFields
          setDisable={setDisable}
          saveClicked={saveClicked}
          setSaveClicked={setSaveClicked}
        />
        <ClientAddresses />
      </ContentWrapper>
    </PageWrapper>
  );
};

export default ClientByIdPage;
