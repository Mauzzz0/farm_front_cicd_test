import React, { useEffect, useState } from 'react';
import PageWrapper from '../../components/ui/pageWrapper/PageWrapper';
import Heading from '../../components/heading/Heading';
import { ContentWrapper } from '../../components/ui/pageWrapper/ContentWrapper';
import { PrimaryBtn } from '../../components/ui/buttons/PrimaryBtn';
import { useLocation, useParams } from 'react-router-dom';
import { useAppDispatch, useAppSelector } from '../../hooks/redux';
import TextField from '../../components/ui/inputs/TextField';
import { SimpleInput } from '../../components/ui/inputs/SimpleInput';
import { Textarea } from '../../components/ui/inputs/Textarea';
import { callByIdResponseModel, CallResponse } from '../../models/Call';
import { getCallById, updateCall } from '../../store/reducers/call/ActionCreators';
import { setLocation } from '../../store/reducers/breadcrumb/ActionCreators';
import { PageFieldsWrapper } from '../../components/ui/pageWrapper/PageFieldsWrapper';

const CallByIdPage = () => {
  const { id } = useParams<{ id: string }>();
  const { pathname } = useLocation();
  const dispatch = useAppDispatch();
  const { call, isLoading } = useAppSelector((state) => state.callReducer);
  const [newCall, setNewCall] = useState<CallResponse>(call);
  const [isDisable, setDisable] = useState(true);

  useEffect(() => {
    if (id) {
      dispatch(getCallById(id));
    }
  }, []);

  useEffect(() => {
    if (!isLoading) {
      setNewCall(call);
      const title = `Заявка по номеру ${call.phone}`;
      dispatch(setLocation(pathname, title));
    }
  }, [call]);

  useEffect(() => {
    JSON.stringify(call) !== JSON.stringify(newCall) ? setDisable(false) : setDisable(true);
  }, [newCall]);

  const onChangeHandler = (e: React.ChangeEvent<HTMLTextAreaElement>, key: keyof CallResponse) => {
    setNewCall((prevState) => ({ ...prevState, [key]: e.target.value }));
  };

  const onSaveHandler = async () => {
    await dispatch(updateCall(newCall));
    window.location.reload();
  };

  return (
    <PageWrapper>
      <Heading>
        <PrimaryBtn size={'medium'} disabled={isDisable} onClick={onSaveHandler}>
          Сохранить
        </PrimaryBtn>
      </Heading>
      <ContentWrapper flex_dir={'column'} flex_wrap={'nowrap'}>
        <PageFieldsWrapper>
          <TextField label={'Статус'}>
            <SimpleInput placeholder={'Не указано'} />
          </TextField>
          <TextField label={'Комментарий сотрудника'}>
            <Textarea
              placeholder={'Не указано'}
              value={newCall.adminComment}
              onChange={(e) => onChangeHandler(e, 'adminComment')}
            />
          </TextField>
          {callByIdResponseModel.slice(2).map((item) => (
            <TextField key={item.name} label={item.title}>
              {item.name !== 'userComment' ? (
                <SimpleInput
                  placeholder={'Не указано'}
                  defaultValue={newCall[item.name].toString()}
                  disabled={true}
                />
              ) : (
                <Textarea
                  placeholder={'Не указано'}
                  defaultValue={newCall[item.name]}
                  disabled={true}
                />
              )}
            </TextField>
          ))}
        </PageFieldsWrapper>
      </ContentWrapper>
    </PageWrapper>
  );
};

export default CallByIdPage;
