import React, { useEffect, useState } from 'react';
import { useAppDispatch, useAppSelector } from '../../hooks/redux';
import { fetchCalls } from '../../store/reducers/call/ActionCreators';
import PageWrapper from '../../components/ui/pageWrapper/PageWrapper';
import Heading from '../../components/heading/Heading';
import { ContentWrapper } from '../../components/ui/pageWrapper/ContentWrapper';
import Table from '../../components/ui/table/Table';
import { callResponseModel, CallStatusEnum, callStatusInfo } from '../../models/Call';
import { setLocation } from '../../store/reducers/breadcrumb/ActionCreators';
import { useLocation } from 'react-router-dom';
import { TableInputs } from '../../components/ui/table/TableInputs';
import { SimpleInput } from '../../components/ui/inputs/SimpleInput';
import TextField from '../../components/ui/inputs/TextField';
import Select from '../../components/ui/inputs/Select';
import { getCallStatusName } from '../../utils';

const CallsPage = () => {
  const dispatch = useAppDispatch();
  const { pathname } = useLocation();
  const { callList } = useAppSelector((state) => state.callReducer);
  const [query, setQuery] = useState('');
  const [done, setDone] = useState<{ name: boolean | undefined; title: string }>();

  useEffect(() => {
    dispatch(fetchCalls(query, done?.name));
    dispatch(setLocation(pathname, ''));
  }, [query, done]);

  const selectHandler = (value: string) => {
    if (value === CallStatusEnum.All) {
      setDone({ name: undefined, title: '' });
    } else if (value in CallStatusEnum) {
      setDone({ name: value === CallStatusEnum.Done, title: getCallStatusName(value) });
    }
  };

  return (
    <PageWrapper>
      <Heading />
      <ContentWrapper flex_dir={'column'} flex_wrap={'wrap'} gap={32}>
        <TableInputs>
          <TextField>
            <SimpleInput
              placeholder={'Поиск'}
              value={query}
              onChange={(e) => setQuery(e.target.value)}
            />
          </TextField>
          <TextField label={callStatusInfo.title}>
            <Select
              placeholder={'Все'}
              value={done?.title ?? ''}
              options={callStatusInfo.options}
              addOptions={[{ name: CallStatusEnum.All, title: 'Все' }]}
              eventHandler={selectHandler}
            />
          </TextField>
        </TableInputs>
        {callList.length ? (
          <Table header={callResponseModel} data={callList} minWidth={170} />
        ) : (
          <span>Пока нет заявок на звонок</span>
        )}
      </ContentWrapper>
    </PageWrapper>
  );
};

export default CallsPage;
