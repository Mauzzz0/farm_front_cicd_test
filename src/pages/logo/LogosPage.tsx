import React, { useEffect, useState } from 'react';
import { useAppDispatch, useAppSelector } from '../../hooks/redux';
import { fetchLogos } from '../../store/reducers/logo/ActionCreators';
import { useLocation } from 'react-router-dom';
import { setLocation } from '../../store/reducers/breadcrumb/ActionCreators';
import PageWrapper from '../../components/ui/pageWrapper/PageWrapper';
import Heading from '../../components/heading/Heading';
import { ContentWrapper } from '../../components/ui/pageWrapper/ContentWrapper';
import { PrimaryBtn } from '../../components/ui/buttons/PrimaryBtn';
import TextField from '../../components/ui/inputs/TextField';
import { SimpleInput } from '../../components/ui/inputs/SimpleInput';
import LogoList from '../../components/logo/LogoList';
import ModalWrapper from '../../components/ui/modals/ModalWrapper';
import LogoCreation from '../../components/ui/modals/logo/LogoCreation';

const LogosPage: React.FC = () => {
  const dispatch = useAppDispatch();
  const { pathname } = useLocation();
  const { logoList } = useAppSelector((state) => state.logoReducer);
  const [visibility, setVisibility] = useState<'hidden' | 'visible'>('hidden');

  useEffect(() => {
    dispatch(fetchLogos());
    dispatch(setLocation(pathname, ''));
  }, []);

  return (
    <PageWrapper>
      <Heading>
        <PrimaryBtn size={'medium'} onClick={() => setVisibility('visible')}>
          Добавить Логотип
        </PrimaryBtn>
      </Heading>
      <ContentWrapper flex_dir={'column'} flex_wrap={'nowrap'} gap={32}>
        <TextField>
          <SimpleInput placeholder={'Поиск'} />
        </TextField>
        <LogoList logos={logoList} />
      </ContentWrapper>
      <ModalWrapper
        visibility={visibility}
        title={'Добавить Иконку для Категории'}
        setVisibility={setVisibility}
      >
        <LogoCreation setVisibility={setVisibility} />
      </ModalWrapper>
    </PageWrapper>
  );
};

export default LogosPage;
