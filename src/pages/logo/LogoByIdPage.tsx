import React, { useEffect, useState } from 'react';
import PageWrapper from '../../components/ui/pageWrapper/PageWrapper';
import Heading from '../../components/heading/Heading';
import { OutlineBtn } from '../../components/ui/buttons/OutlineBtn';
import { PrimaryBtn } from '../../components/ui/buttons/PrimaryBtn';
import { ContentWrapper } from '../../components/ui/pageWrapper/ContentWrapper';
import { useLocation, useNavigate, useParams } from 'react-router-dom';
import { useAppDispatch, useAppSelector } from '../../hooks/redux';
import { LogoResponse, logoResponseModel } from '../../models/Logo';
import { deleteLogo, getLogoById, updateLogo } from '../../store/reducers/logo/ActionCreators';
import { setLocation } from '../../store/reducers/breadcrumb/ActionCreators';
import TextField from '../../components/ui/inputs/TextField';
import { SimpleInput } from '../../components/ui/inputs/SimpleInput';
import { PageFieldsWrapper } from '../../components/ui/pageWrapper/PageFieldsWrapper';
import styled from 'styled-components';
import { pxToRem } from '../../utils/Converting';

const ImgLine = styled.div`
  display: flex;
  align-items: center;
  gap: ${pxToRem(48)}rem;

  align-self: stretch;
`;

const Img = styled.img`
  height: ${pxToRem(120)}rem;
  width: ${pxToRem(120)}rem;
`;

const LogoByIdPage: React.FC = () => {
  const { id } = useParams<{ id: string }>();
  const { pathname } = useLocation();
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const { logo, isLoading } = useAppSelector((state) => state.logoReducer);
  const [newLogo, setNewLogo] = useState<LogoResponse>(logo);
  const [isDisable, setDisable] = useState<boolean>(true);

  useEffect(() => {
    if (id) {
      dispatch(getLogoById(id));
    }
  }, []);

  useEffect(() => {
    if (!isLoading) {
      setNewLogo(logo);
      const title = `№${logo.id} ${logo.groupName}`;
      dispatch(setLocation(pathname, title));
    }
  }, [logo]);

  useEffect(() => {
    JSON.stringify(logo) !== JSON.stringify(newLogo) ? setDisable(false) : setDisable(true);
  }, [newLogo]);

  const onChangeHandler = (e: React.ChangeEvent<HTMLInputElement>, key: keyof LogoResponse) => {
    setNewLogo((prevState) => ({ ...prevState, [key]: e.target.value }));
  };

  const onSaveHandler = async () => {
    await dispatch(updateLogo(newLogo.id, { url: newLogo.url }));
    window.location.reload();
  };

  const onDeleteHandler = async () => {
    await dispatch(deleteLogo(newLogo.id));
    navigate(-1);
  };

  return (
    <PageWrapper>
      <Heading>
        <OutlineBtn size={'medium'} onClick={() => onDeleteHandler()}>
          Удалить
        </OutlineBtn>
        <PrimaryBtn size={'medium'} disabled={isDisable} onClick={() => onSaveHandler()}>
          Сохранить
        </PrimaryBtn>
      </Heading>
      <ContentWrapper flex_dir={'column'} flex_wrap={'nowrap'} gap={48}>
        <PageFieldsWrapper>
          {logoResponseModel.slice(0, 2).map((item) => (
            <TextField key={item.name} label={item.title}>
              <SimpleInput placeholder={'Не указано'} value={newLogo[item.name]} disabled={true} />
            </TextField>
          ))}
        </PageFieldsWrapper>
        <ImgLine>
          <TextField label={logoResponseModel[2].title} width={'100%'}>
            <SimpleInput
              placeholder={'Не указано'}
              value={newLogo.url}
              onChange={(e) => onChangeHandler(e, 'url')}
            />
          </TextField>
          <Img src={newLogo.url} />
        </ImgLine>
      </ContentWrapper>
    </PageWrapper>
  );
};

export default LogoByIdPage;
