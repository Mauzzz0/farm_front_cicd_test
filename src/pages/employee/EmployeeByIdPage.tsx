import React, { useEffect, useState } from 'react';
import PageWrapper from '../../components/ui/pageWrapper/PageWrapper';
import Heading from '../../components/heading/Heading';
import { ContentWrapper } from '../../components/ui/pageWrapper/ContentWrapper';
import { PrimaryBtn } from '../../components/ui/buttons/PrimaryBtn';
import TextField from '../../components/ui/inputs/TextField';
import { SimpleInput } from '../../components/ui/inputs/SimpleInput';
import { useAppDispatch, useAppSelector } from '../../hooks/redux';
import { getEmployeeByID, updateEmployee } from '../../store/reducers/employee/ActionCreators';
import { useLocation, useParams } from 'react-router-dom';
import {
  employeeResponseModel,
  IRoleInfo,
  IStatusInfo,
  roleInfo,
  statusInfo,
} from '../../models/EmployeeModel';
import Select from '../../components/ui/inputs/Select';
import { EmployeeResponse, RoleEnum, StatusEnum } from '../../models/Employee';
import { setLocation } from '../../store/reducers/breadcrumb/ActionCreators';
import { getCurrentRoleName, getCurrentStatusName } from '../../utils';
import { PageFieldsWrapper } from '../../components/ui/pageWrapper/PageFieldsWrapper';

const EmployeeByIdPage: React.FC = () => {
  const dispatch = useAppDispatch();
  const { id } = useParams<{ id: string }>();
  const { employee, isLoading } = useAppSelector((state) => state.employeeReducer);
  const [newEmployee, setNewEmployee] = useState<EmployeeResponse>(employee);
  const [loading, setLoading] = useState(true);
  const { pathname } = useLocation();
  const [isDisable, setDisable] = useState(true);

  useEffect(() => {
    if (id) {
      dispatch(getEmployeeByID(id));
      setLoading(isLoading);
    }
  }, []);

  useEffect(() => {
    if (!loading) {
      setNewEmployee(employee);
      const title = `${employee.surname} ${employee.name}`;
      dispatch(setLocation(pathname, title));
    }
  }, [employee]);

  useEffect(() => {
    if (JSON.stringify(employee) !== JSON.stringify(newEmployee)) {
      setDisable(false);
    } else {
      setDisable(true);
    }
  }, [newEmployee]);

  const onChangeHandler = (e: React.ChangeEvent<HTMLInputElement>, key: keyof EmployeeResponse) => {
    setNewEmployee((prevState) => ({ ...prevState, [key]: e.target.value.trim() }));
  };

  const selectHandler = (value: string) => {
    let changes = {};

    if (value in RoleEnum) {
      changes = { role: value };
    } else {
      changes = { blocked: value === StatusEnum.Blocked };
    }

    setNewEmployee((prevState) => ({ ...prevState, ...changes }));
  };

  const onSaveHandler = async () => {
    await dispatch(updateEmployee(newEmployee));
    window.location.reload();
  };

  const getCurrentValue = (item: IRoleInfo | IStatusInfo): string => {
    if (item.name === 'role') {
      return getCurrentRoleName(newEmployee);
    }
    if (item.name === 'blocked') {
      return getCurrentStatusName(newEmployee);
    }
    return '';
  };

  return (
    <PageWrapper>
      <Heading>
        {/*<OutlineBtn size={'medium'}>Удалить</OutlineBtn>*/}
        <PrimaryBtn size={'medium'} disabled={isDisable} onClick={() => onSaveHandler()}>
          Сохранить
        </PrimaryBtn>
      </Heading>
      <ContentWrapper flex_dir={'row'} flex_wrap={'wrap'} gap={42}>
        <PageFieldsWrapper>
          {employeeResponseModel.slice(1, 5).map((item) => (
            <TextField key={item.name} label={item.title}>
              <SimpleInput
                placeholder={'Не указано'}
                value={(newEmployee[item.name] ?? '').toString()}
                onChange={(e) => onChangeHandler(e, item.name)}
                disabled={item.name === 'phone'}
              />
            </TextField>
          ))}
          {[roleInfo, statusInfo].map((item) => (
            <TextField key={item.title} label={item.title}>
              <Select
                placeholder={''}
                options={item.options}
                eventHandler={selectHandler}
                value={getCurrentValue(item)}
              />
            </TextField>
          ))}
        </PageFieldsWrapper>
      </ContentWrapper>
    </PageWrapper>
  );
};

export default EmployeeByIdPage;
