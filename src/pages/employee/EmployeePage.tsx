import React, { useEffect, useState } from 'react';
import Heading from '../../components/heading/Heading';
import { PrimaryBtn } from '../../components/ui/buttons/PrimaryBtn';
import PageWrapper from '../../components/ui/pageWrapper/PageWrapper';
import { ContentWrapper } from '../../components/ui/pageWrapper/ContentWrapper';
import { TableInputs } from '../../components/ui/table/TableInputs';
import TextField from '../../components/ui/inputs/TextField';
import { SimpleInput } from '../../components/ui/inputs/SimpleInput';
import { fetchEmployees } from '../../store/reducers/employee/ActionCreators';
import { useAppDispatch, useAppSelector } from '../../hooks/redux';
import ModalWrapper from '../../components/ui/modals/ModalWrapper';
import CreateEmployee from '../../components/ui/modals/employee/CreateEmployee';
import { employeeResponseModel, roleInfo, statusInfo } from '../../models/EmployeeModel';
import { setLocation } from '../../store/reducers/breadcrumb/ActionCreators';
import { useLocation } from 'react-router-dom';
import { RoleEnum, StatusEnum } from '../../models/Employee';
import Table from '../../components/ui/table/Table';
import { getSelectedRoleName, getSelectedStatusName } from '../../utils';
import MultipleAutocomplete from '../../components/ui/inputs/MultipleAutocomplete';
import Select from '../../components/ui/inputs/Select';

const EmployeePage: React.FC = () => {
  const { pathname } = useLocation();
  const dispatch = useAppDispatch();
  const { employeeList } = useAppSelector((state) => state.employeeReducer);
  const [visibility, setVisibility] = useState<'hidden' | 'visible'>('hidden');
  const initRoles = roleInfo.options.map((role) => {
    return {
      name: role.name,
      title: role.title,
      bg: role.bg,
    };
  });
  const [roles, setRoles] = useState<{ name: RoleEnum; title: string; bg: string }[]>(initRoles);
  const [blocked, setBlocked] = useState<{ name: boolean | undefined; title: string }>();
  const [query, setQuery] = useState<string>();
  const [employeeValues, setEmployeeValues] = useState<(string | JSX.Element)[][]>([]);

  useEffect(() => {
    dispatch(
      fetchEmployees(
        roles.map((role) => role.name),
        query,
        blocked?.name,
      ),
    );
    dispatch(setLocation(pathname, ''));
  }, [roles, query, blocked]);

  useEffect(() => {
    if (employeeList.length !== 0) {
      convertList();
    }
  }, [employeeList]);

  const convertList = () => {
    setEmployeeValues([]);

    const newClientValues = employeeList.reduce((prev, cur) => {
      const modelData = employeeResponseModel.map((key) => {
        return cur[key.name];
      });

      return [...prev, modelData];
    }, [] as any[]);

    setEmployeeValues(newClientValues);
  };

  const deleteRole = (key: string) => {
    if (roles.length > 1) {
      const changes = roles.filter((role) => role.name !== key);
      setRoles(changes);
    }
  };

  const autocompleteHandler = (value: string) => {
    if (!roles.find((role) => role.name === value)) {
      const changes = {
        name: value as RoleEnum,
        title: getSelectedRoleName(value),
        bg: roleInfo.options.find((item) => item.name === value)?.bg ?? '',
      };
      setRoles((prevState) => [...prevState, changes]);
    }
  };

  const selectHandler = (value: string) => {
    if (value === StatusEnum.AllStatuses) {
      setBlocked({ name: undefined, title: '' });
      return;
    }
    setBlocked({
      name: value === StatusEnum.Blocked,
      title: getSelectedStatusName(value),
    });
  };

  return (
    <PageWrapper>
      <Heading>
        <PrimaryBtn size={'medium'} onClick={() => setVisibility('visible')}>
          Добавить Сотрудника
        </PrimaryBtn>
      </Heading>
      <ContentWrapper flex_dir={'column'} flex_wrap={'nowrap'} gap={32}>
        <TableInputs>
          <TextField>
            <SimpleInput
              placeholder={'Поиск'}
              value={query}
              onChange={(e) => setQuery(e.target.value)}
            />
          </TextField>
          {[roleInfo, statusInfo].map((item) => (
            <TextField width={item.name === 'role' ? '21.25rem' : undefined} label={item.title}>
              {item.name === 'role' ? (
                <MultipleAutocomplete
                  options={roleInfo.options}
                  eventHandler={autocompleteHandler}
                  onDeleteHandler={deleteRole}
                  values={roles}
                />
              ) : (
                <Select
                  placeholder={'Все'}
                  options={statusInfo.options}
                  addOptions={[{ name: StatusEnum.AllStatuses, title: 'Все' }]}
                  eventHandler={selectHandler}
                  value={blocked?.title ?? ''}
                />
              )}
            </TextField>
          ))}
        </TableInputs>
        {employeeList.length ? (
          <Table header={employeeResponseModel} data={employeeValues} minWidth={148} />
        ) : (
          <span>Пока нет сотрудников</span>
        )}
        <ModalWrapper
          title={'Создание Сотрудника'}
          visibility={visibility}
          setVisibility={setVisibility}
        >
          <CreateEmployee setVisibility={setVisibility} />
        </ModalWrapper>
      </ContentWrapper>
    </PageWrapper>
  );
};

export default EmployeePage;
