import React, { useEffect, useState } from 'react';
import PageWrapper from '../../components/ui/pageWrapper/PageWrapper';
import Heading from '../../components/heading/Heading';
import { ContentWrapper } from '../../components/ui/pageWrapper/ContentWrapper';
import { useLocation, useNavigate, useParams } from 'react-router-dom';
import { useAppDispatch, useAppSelector } from '../../hooks/redux';
import { PharmResponse, pharmResponseModel } from '../../models/Pharmacy';
import {
  deletePharm,
  getPharmById,
  updatePharm,
} from '../../store/reducers/pharmacy/ActionCreators';
import { setLocation } from '../../store/reducers/breadcrumb/ActionCreators';
import { PrimaryBtn } from '../../components/ui/buttons/PrimaryBtn';
import { OutlineBtn } from '../../components/ui/buttons/OutlineBtn';
import TextField from '../../components/ui/inputs/TextField';
import { SimpleInput } from '../../components/ui/inputs/SimpleInput';
import { Textarea } from '../../components/ui/inputs/Textarea';
import { PageFieldsWrapper } from '../../components/ui/pageWrapper/PageFieldsWrapper';
import PharmImgList from '../../components/pharm/PharmImgList';

const PharmByIdPage: React.FC = () => {
  const { id } = useParams();
  const { pathname } = useLocation();
  const dispatch = useAppDispatch();
  const { pharm, isLoading } = useAppSelector((state) => state.pharmReducer);
  const [newPharm, setNewPharm] = useState<PharmResponse>(pharm);
  const [isDisabled, setDisabled] = useState(true);
  const navigate = useNavigate();

  useEffect(() => {
    if (id) {
      dispatch(getPharmById(id));
    }
  }, []);

  useEffect(() => {
    if (!isLoading) {
      setNewPharm(pharm);
      dispatch(setLocation(pathname, pharm.name));
    }
  }, [pharm]);

  useEffect(() => {
    JSON.stringify(pharm) !== JSON.stringify(newPharm) ? setDisabled(false) : setDisabled(true);
  }, [newPharm]);

  const onChangeHandler = (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
    key: keyof PharmResponse,
  ) => {
    setNewPharm((prevState) => ({ ...prevState, [key]: e.target.value }));
  };

  const addImg = (idx: number, value?: string) => {
    const changes = [...newPharm.icon];
    changes[idx] = value ?? '';
    setNewPharm((prevState) => ({ ...prevState, icon: changes }));
  };

  const deleteImg = (idx: number) => {
    const changes = newPharm.icon;
    changes.splice(idx, 1);
    setNewPharm((prevState) => ({ ...prevState, icon: changes }));
  };

  const onSaveHandler = async () => {
    await dispatch(updatePharm(newPharm));
    window.location.reload();
  };

  const onDeleteHandler = async () => {
    if (id) {
      await dispatch(deletePharm(id));
      navigate(-1);
    }
  };

  return (
    <PageWrapper>
      <Heading>
        <OutlineBtn size={'medium'} onClick={() => onDeleteHandler()}>
          Удалить
        </OutlineBtn>
        <PrimaryBtn size={'medium'} disabled={isDisabled} onClick={() => onSaveHandler()}>
          Сохранить
        </PrimaryBtn>
      </Heading>
      <ContentWrapper flex_dir={'column'} flex_wrap={'nowrap'} gap={64}>
        <PageFieldsWrapper>
          {pharmResponseModel.slice(1).map((item) => (
            <TextField
              key={item.name}
              label={item.title}
              width={
                item.name === 'map'
                  ? '25rem'
                  : item.name === 'lat' || item.name === 'lon'
                  ? '15.625rem'
                  : ''
              }
            >
              {item.name === 'works' ? (
                <Textarea
                  placeholder={'Не указано'}
                  value={newPharm[item.name] ?? ''}
                  onChange={(e) => onChangeHandler(e, item.name)}
                />
              ) : (
                <SimpleInput
                  placeholder={'Не указано'}
                  value={newPharm[item.name] ?? ''}
                  onChange={(e) => onChangeHandler(e, item.name)}
                />
              )}
            </TextField>
          ))}
        </PageFieldsWrapper>
        <PharmImgList linkList={newPharm.icon} addImg={addImg} deleteImg={deleteImg} />
      </ContentWrapper>
    </PageWrapper>
  );
};

export default PharmByIdPage;
