import React, { useEffect, useState } from 'react';
import PageWrapper from '../../components/ui/pageWrapper/PageWrapper';
import Heading from '../../components/heading/Heading';
import { ContentWrapper } from '../../components/ui/pageWrapper/ContentWrapper';
import { useLocation } from 'react-router-dom';
import { useAppDispatch, useAppSelector } from '../../hooks/redux';
import { fetchPharms } from '../../store/reducers/pharmacy/ActionCreators';
import { setLocation } from '../../store/reducers/breadcrumb/ActionCreators';
import Table from '../../components/ui/table/Table';
import { pharmResponseModel } from '../../models/Pharmacy';
import ModalWrapper from '../../components/ui/modals/ModalWrapper';
import PharmCreation from '../../components/ui/modals/pharm/PharmCreation';
import { PrimaryBtn } from '../../components/ui/buttons/PrimaryBtn';

const PharmPage: React.FC = () => {
  const { pathname } = useLocation();
  const dispatch = useAppDispatch();
  const { pharmList } = useAppSelector((state) => state.pharmReducer);
  const [visibility, setVisibility] = useState<'hidden' | 'visible'>('hidden');

  useEffect(() => {
    dispatch(fetchPharms());
    dispatch(setLocation(pathname, ''));
  }, []);

  return (
    <PageWrapper>
      <Heading>
        <PrimaryBtn size={'medium'} onClick={() => setVisibility('visible')}>
          Добавить Аптеку
        </PrimaryBtn>
      </Heading>
      <ContentWrapper flex_dir={'column'} flex_wrap={'wrap'}>
        {pharmList.length ? (
          <Table header={pharmResponseModel} data={pharmList} minWidth={300} />
        ) : (
          <span>Пока нет ни одной аптеки</span>
        )}
      </ContentWrapper>
      <ModalWrapper visibility={visibility} title={'Добавить Аптеку'} setVisibility={setVisibility}>
        <PharmCreation setVisibility={setVisibility} />
      </ModalWrapper>
    </PageWrapper>
  );
};

export default PharmPage;
