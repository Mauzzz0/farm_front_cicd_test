import AuthPage from '../pages/AuthPage';
import EmployeePage from '../pages/employee/EmployeePage';
import EmployeeByIdPage from '../pages/employee/EmployeeByIdPage';
import React, { ReactNode } from 'react';
import ClientsPage from '../pages/client/ClientsPage';
import ClientByIdPage from '../pages/client/ClientByIdPage';
import CallsPage from '../pages/call/CallsPage';
import CallByIdPage from '../pages/call/CallByIdPage';
import PharmPage from '../pages/pharm/PharmPage';
import PharmByIdPage from '../pages/pharm/PharmByIdPage';
import LogosPage from '../pages/logo/LogosPage';
import LogoByIdPage from '../pages/logo/LogoByIdPage';

export const authRoutes = [
  { path: '/auth', element: <AuthPage /> },
  { path: '/', element: <AuthPage /> },
  { path: '*', element: <AuthPage /> },
];

export enum pathEnum {
  clients = '/clients',
  employees = '/employees',
  orders = '/orders',
  calls = '/calls',
  pharmacies = '/pharmacies',
  logos = '/logos',
}

interface IMainRoutes {
  name: string;
  path: string;
  element: ReactNode;
}

export const mainRoutes: IMainRoutes[] = [
  { name: 'Клиенты', path: pathEnum.clients, element: <ClientsPage /> },
  { name: '', path: `${pathEnum.clients}/:id`, element: <ClientByIdPage /> },
  { name: 'Сотрудники', path: pathEnum.employees, element: <EmployeePage /> },
  { name: '', path: `${pathEnum.employees}/:id`, element: <EmployeeByIdPage /> },
  { name: 'Заказы', path: pathEnum.orders, element: <></> },
  { name: 'Звонки', path: pathEnum.calls, element: <CallsPage /> },
  { name: '', path: `${pathEnum.calls}/:id`, element: <CallByIdPage /> },
  { name: 'Аптеки', path: pathEnum.pharmacies, element: <PharmPage /> },
  { name: '', path: `${pathEnum.pharmacies}/:id`, element: <PharmByIdPage /> },
  { name: 'Логотипы', path: pathEnum.logos, element: <LogosPage /> },
  { name: '', path: `${pathEnum.logos}/:id`, element: <LogoByIdPage /> },
  { name: 'Not Found', path: '*', element: <EmployeePage /> },
];
