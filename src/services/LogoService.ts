import { AxiosResponse } from 'axios';
import {
  ICategoryResponse,
  ILogoByIdResponse,
  ILogosResponse,
  LogoInitResponse,
  LogoUpdate,
} from '../models/Logo';
import api from '../api';

export class LogoService {
  static async fetchLogos(): Promise<AxiosResponse<ILogosResponse>> {
    return api.get<ILogosResponse>(`category-logo?cacheIgnore=true`);
  }

  static async fetchCategories(): Promise<AxiosResponse<ICategoryResponse>> {
    return api.get<ICategoryResponse>(`catalog/categories`);
  }

  static async getLogoById(id: string): Promise<AxiosResponse<ILogoByIdResponse>> {
    return api.get<ILogoByIdResponse>(`category-logo/${id}?cacheIgnore=true`);
  }

  static async updateLogo(id: number, data: LogoUpdate): Promise<AxiosResponse<ILogoByIdResponse>> {
    return api.put<ILogoByIdResponse>(`category-logo/${id}`, data);
  }

  static async deleteLogo(id: number): Promise<AxiosResponse<ILogoByIdResponse>> {
    return api.delete<ILogoByIdResponse>(`category-logo/${id}`);
  }

  static async postLogo(data: LogoInitResponse): Promise<AxiosResponse<ILogoByIdResponse>> {
    return api.post<ILogoByIdResponse>(`category-logo`, data);
  }
}
