import { AxiosResponse } from 'axios';
import { IPharmByIdResponse, IPharmResponse, PharmResponse, PharmUpdate } from '../models/Pharmacy';
import api from '../api';

export class PharmService {
  static async fetchPharms(): Promise<AxiosResponse<IPharmResponse>> {
    return api.get<IPharmResponse>(`pharmacy?cacheIgnore=true`);
  }

  static async getPharmById(id: string): Promise<AxiosResponse<IPharmByIdResponse>> {
    return api.get<IPharmByIdResponse>(`pharmacy/${id}?cacheIgnore=true`);
  }

  static async updatePharm(
    id: string,
    data: PharmUpdate,
  ): Promise<AxiosResponse<IPharmByIdResponse>> {
    return api.put<IPharmByIdResponse>(`pharmacy/${id}`, data);
  }

  static async postPharm(data: PharmResponse): Promise<AxiosResponse<IPharmByIdResponse>> {
    return api.post<IPharmByIdResponse>(`pharmacy`, data);
  }

  static async deletePharm(id: string): Promise<AxiosResponse<IPharmByIdResponse>> {
    return api.delete<IPharmByIdResponse>(`pharmacy/${id}`);
  }
}
