import { AxiosResponse } from 'axios';
import api from '../api';
import { ClientByIdResponse, IClientResponse, UpdateClient } from '../models/ClientModel';
import { ClientAddressUpdate, IClientAddressesResponse } from '../models/ClientAddress';

export default class ClientService {
  static async getClients(
    query?: string,
    blocked?: boolean,
  ): Promise<AxiosResponse<IClientResponse>> {
    return api.get<IClientResponse>(
      `/admin/user?role=User${query ? `&value=${query}` : ''}${
        blocked !== undefined ? `&blocked=${blocked}` : ''
      }`,
    );
  }

  static async getClientById(id: string): Promise<AxiosResponse<ClientByIdResponse>> {
    return api.get<ClientByIdResponse>(`/admin/user/${id}`);
  }

  static async updateClient(
    id: string,
    data: UpdateClient,
  ): Promise<AxiosResponse<IClientResponse>> {
    return api.put<IClientResponse>(`/admin/user/${id}`, data);
  }

  static async updateClientAddress(
    userId: string,
    addressId: string,
    data: ClientAddressUpdate,
  ): Promise<AxiosResponse<IClientAddressesResponse>> {
    return api.put<IClientAddressesResponse>(`/user/address/${addressId}?auserid=${userId}`, data);
  }
}
