import { AxiosResponse } from 'axios';
import { CallUpdate, ICallByIdResponse, ICallResponse } from '../models/Call';
import api from '../api';

export default class CallService {
  static async fetchCalls(query?: string, done?: boolean): Promise<AxiosResponse<ICallResponse>> {
    return api.get<ICallResponse>(
      `/utility/call?${query ? 'value=' + query : ''}${done !== undefined ? '&done=' + done : ''}`,
    );
  }

  static async getCallById(id: string): Promise<AxiosResponse<ICallByIdResponse>> {
    return api.get<ICallByIdResponse>(`/utility/call/${id}`);
  }

  static async updateCall(id: string, data: CallUpdate): Promise<AxiosResponse<ICallByIdResponse>> {
    return api.put<ICallByIdResponse>(`/utility/call/${id}`, data);
  }
}
