import { ClientAddressResponse } from './ClientAddress';

export class UpdateClient {
  public name: string;
  public surname: string;
  public patronymic: string;
  public blocked: boolean;
}

export class ClientResponse extends UpdateClient {
  public id: string;
  public phone: string;
  public addresses: ClientAddressResponse[];
}

export interface IClientResponse {
  payload: { items: ClientResponse[] };
}

export interface ClientByIdResponse {
  payload: ClientResponse;
}

export const clientResponseModel: { name: keyof ClientResponse; title: string }[] = [
  { name: 'id', title: 'id' },
  { name: 'surname', title: 'Фамилия' },
  { name: 'name', title: 'Имя' },
  { name: 'patronymic', title: 'Отчество' },
  { name: 'phone', title: 'Телефон' },
  { name: 'blocked', title: 'Статус' },
];
