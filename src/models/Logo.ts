export class LogoUpdate {
  public url: string;
}

export class LogoInitResponse extends LogoUpdate {
  public id: number;
}

export class CategoryResponse {
  public groupId: number;
  public groupName: string;
}

export class LogoResponse extends LogoInitResponse {
  public groupName: string;
}

export interface ILogosResponse {
  payload: { items: LogoInitResponse[] };
}

export interface ICategoryResponse {
  payload: { list: CategoryResponse[] };
}

export interface ILogoByIdResponse {
  payload: LogoInitResponse;
}

export const logoCreateModel: { name: keyof LogoInitResponse; title: string }[] = [
  { name: 'id', title: 'id' },
  { name: 'url', title: 'Сылка на иконку' },
];

export const logoResponseModel: { name: keyof LogoResponse; title: string }[] = [
  logoCreateModel[0],
  { name: 'groupName', title: 'Название' },
  logoCreateModel[1],
];
