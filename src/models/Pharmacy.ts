export class PharmUpdate {
  public name: string;
  public phone: string;
  public works: string;
  public map: string;
  public lat: number;
  public lon: number;
  public icon: string[];
}

export class PharmResponse extends PharmUpdate {
  public id: string;
}

export interface IPharmResponse {
  payload: { items: PharmResponse[] };
}

export interface IPharmByIdResponse {
  payload: PharmResponse;
}

export const pharmResponseModel: { name: keyof PharmResponse; title: string }[] = [
  { name: 'id', title: 'id' },
  { name: 'name', title: 'Название' },
  { name: 'phone', title: 'Телефон' },
  { name: 'works', title: 'График работы' },
  { name: 'map', title: 'Ссылка на карту' },
  { name: 'lat', title: 'Широта' },
  { name: 'lon', title: 'Долгота' },
];
