export class ClientAddressUpdate {
  public region: string;
  public city: string;
  public street: string;
  public house: string;
  public entrance: string;
  public floor: string;
  public apartments: string;
  public comment: string;
}

export class ClientAddressResponse extends ClientAddressUpdate {
  public id: string;
}

export interface IClientAddressesResponse {
  payload: { items: ClientAddressResponse[] };
}

export const AddressModelUpdate: { name: keyof ClientAddressUpdate; title: string }[] = [
  { name: 'region', title: 'Регион' },
  { name: 'city', title: 'Город' },
  { name: 'street', title: 'Улица' },
  { name: 'house', title: 'Дом' },
  { name: 'entrance', title: 'Подъезд' },
  { name: 'floor', title: 'Этаж' },
  { name: 'apartments', title: 'Квартира/Офис' },
  { name: 'comment', title: 'Комментарий' },
];

export const AddressModelResponse: { name: keyof ClientAddressResponse; title: string }[] = [
  { name: 'id', title: 'id' },
  ...AddressModelUpdate,
];
