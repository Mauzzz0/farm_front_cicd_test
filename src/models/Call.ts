import doneIcon from '../assets/icons/check.svg';
import clockIcon from '../assets/icons/clock.svg';

export class CallUpdate {
  public done: boolean;
  public adminComment: string;
}

export class CallResponse extends CallUpdate {
  public id: string;
  public name: string;
  public phone: string;
  public userComment: string;
  public created: string;
  public doneAt: string;
}

export interface ICallResponse {
  payload: { items: CallResponse[] };
}

export interface ICallByIdResponse {
  payload: CallResponse;
}

export const callResponseModel: { name: keyof CallResponse; title: string }[] = [
  { name: 'id', title: 'id' },
  { name: 'name', title: 'ФИО' },
  { name: 'phone', title: 'Телефон' },
  { name: 'created', title: 'Дата создания заявки' },
  { name: 'doneAt', title: 'Дата обработки' },
  { name: 'userComment', title: 'Комментарий клиента' },
  { name: 'adminComment', title: 'Комментарий сотрудника' },
  { name: 'done', title: 'Статус' },
];

export const callByIdResponseModel: { name: keyof CallResponse; title: string }[] = [
  { name: 'id', title: 'Статус' },
  { name: 'done', title: 'id' },
  { name: 'adminComment', title: 'Комментарий сотрудника' },
  { name: 'created', title: 'Дата создания заявки' },
  { name: 'doneAt', title: 'Дата обработки' },
  { name: 'name', title: 'ФИО' },
  { name: 'phone', title: 'Телефон' },
  { name: 'userComment', title: 'Комментарий клиента' },
];

export enum CallStatusEnum {
  All = 'All',
  Done = 'Done',
  Waiting = 'Waiting',
}

interface IStatusOptions {
  name: CallStatusEnum;
  title: string;
  bg: string;
  icon: string;
}

interface ICallInfo {
  name: keyof CallUpdate;
  title: string;
  options: IStatusOptions[];
  width: number;
}
export const callStatusInfo: ICallInfo = {
  name: 'done',
  title: 'Статус',
  options: [
    { name: CallStatusEnum.Done, title: 'Обработан', bg: '#CBFCD0', icon: doneIcon },
    { name: CallStatusEnum.Waiting, title: 'Ожидается', bg: '#FEC6C6', icon: clockIcon },
  ],
  width: 160,
};
