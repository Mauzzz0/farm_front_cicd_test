export enum RoleEnum {
  AllRoles = 'AllRoles',
  SAdmin = 'SAdmin',
  Admin = 'Admin',
  Courier = 'Courier',
}

export enum StatusEnum {
  AllStatuses = 'AllStatuses',
  Active = 'Active',
  Blocked = 'Blocked',
}

export class EmployeeRequest {
  public phone: string;
  public name: string;
  public surname: string;
  public patronymic: string;
  public role: RoleEnum;
}

export class EmployeeResponse extends EmployeeRequest {
  public id: string;
  public blocked: boolean;
}

export class UpdateEmployee {
  public name: string;
  public surname: string;
  public patronymic: string;
  public role: string;
  public blocked: boolean;
}

export interface IEmployeeResponse {
  payload: { items: EmployeeResponse[] };
}

export interface EmployeeByIdResponse {
  payload: EmployeeResponse;
}

export interface IUpdateEmployee {
  payload: UpdateEmployee;
}
