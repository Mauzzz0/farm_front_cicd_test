import React from 'react';
import { useAppSelector } from '../../hooks/redux';
import AddressById from './AddressById';
import styled from 'styled-components';
import { pxToRem } from '../../utils/Converting';
import { getTypography } from '../../theme/typography';
import { text_colors } from '../../theme/colors';

const SectionWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  gap: ${pxToRem(32)}rem;
  align-self: stretch;

  ${getTypography('header1')};

  color: ${text_colors.dark_grey};
`;

const AddressList = styled.div`
  display: flex;
  flex-direction: column;
  padding: 0 ${pxToRem(12)}rem;
  gap: ${pxToRem(16)}rem;

  align-self: stretch;
`;

const ClientAddresses = () => {
  const { client } = useAppSelector((state) => state.clientReducer);
  return client.addresses.length ? (
    <SectionWrapper>
      Адреса
      <AddressList>
        {client.addresses.map((address) => (
          <AddressById key={address.id} address={address} clientId={client.id} />
        ))}
      </AddressList>
    </SectionWrapper>
  ) : (
    <SectionWrapper>У пользователя нет сохранённх адресов</SectionWrapper>
  );
};

export default ClientAddresses;
