import React, { useEffect, useState } from 'react';
import TextField from '../ui/inputs/TextField';
import { SimpleInput } from '../ui/inputs/SimpleInput';
import { Textarea } from '../ui/inputs/Textarea';
import {
  AddressModelUpdate,
  ClientAddressResponse,
  ClientAddressUpdate,
} from '../../models/ClientAddress';
import Accordion from '../ui/accordions/Accordion';
import { useAppDispatch } from '../../hooks/redux';
import { updateClientAddress } from '../../store/reducers/client/ActionCreators';
import { PageFieldsWrapper } from '../ui/pageWrapper/PageFieldsWrapper';

interface IAddressById {
  address: ClientAddressResponse;
  clientId: string;
}

const AddressById: React.FC<IAddressById> = ({ address, clientId }) => {
  const dispatch = useAppDispatch();
  const title = `${address.city}, ${address.street}, ${address.house}`;
  const [isDisable, setDisable] = useState(true);
  const [newAddress, setNewAddress] = useState<ClientAddressUpdate>(address);

  useEffect(() => {
    setNewAddress(address);
  }, [address]);

  useEffect(() => {
    if (JSON.stringify(address) !== JSON.stringify(newAddress)) {
      setDisable(false);
    } else {
      setDisable(true);
    }
  }, [newAddress]);

  const onChangeHandler = (
    e: React.ChangeEvent<HTMLInputElement>,
    key: keyof ClientAddressResponse,
  ) => {
    setNewAddress((prevState) => ({ ...prevState, [key]: e.target.value.trim() }));
  };

  const onSaveHandler = async () => {
    await dispatch(updateClientAddress(clientId, address.id, newAddress));
    window.location.reload();
  };

  return (
    <Accordion title={title} isDisable={isDisable} onSaveHandler={onSaveHandler}>
      <PageFieldsWrapper>
        {AddressModelUpdate.map((item) => (
          <TextField key={item.name} label={item.title}>
            {item.name === 'comment' ? (
              <Textarea placeholder={'Отсутствует'} defaultValue={newAddress['comment']} disabled />
            ) : (
              <SimpleInput
                placeholder={'Не указано'}
                value={newAddress[item.name]}
                onChange={(e) => onChangeHandler(e, item.name)}
                disabled={item.name === 'region'}
              />
            )}
          </TextField>
        ))}
      </PageFieldsWrapper>
    </Accordion>
  );
};

export default AddressById;
