import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { pxToRem } from '../../utils/Converting';
import { getTypography } from '../../theme/typography';
import { text_colors } from '../../theme/colors';
import { ClientResponse, clientResponseModel } from '../../models/ClientModel';
import TextField from '../ui/inputs/TextField';
import { SimpleInput } from '../ui/inputs/SimpleInput';
import { useAppDispatch, useAppSelector } from '../../hooks/redux';
import { getCurrentStatusName } from '../../utils';
import { IRoleInfo, IStatusInfo, statusInfo } from '../../models/EmployeeModel';
import { StatusEnum } from '../../models/Employee';
import Select from '../ui/inputs/Select';
import { useLocation } from 'react-router-dom';
import { setLocation } from '../../store/reducers/breadcrumb/ActionCreators';
import { updateClient } from '../../store/reducers/client/ActionCreators';
import { PageFieldsWrapper } from '../ui/pageWrapper/PageFieldsWrapper';

const SectionWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  gap: ${pxToRem(32)}rem;
  align-self: stretch;

  ${getTypography('header1')};

  color: ${text_colors.dark_grey};
`;

interface IClientFields {
  setDisable: (b: boolean) => void;
  saveClicked: boolean;
  setSaveClicked: (b: boolean) => void;
}

const ClientFields: React.FC<IClientFields> = ({ setDisable, saveClicked, setSaveClicked }) => {
  const { pathname } = useLocation();
  const dispatch = useAppDispatch();
  const { client, isLoading } = useAppSelector((state) => state.clientReducer);
  const [newClient, setNewClient] = useState<ClientResponse>(client);

  useEffect(() => {
    if (!isLoading) {
      setNewClient(client);
      const title = `${client.surname} ${client.name}`;
      dispatch(setLocation(pathname, title));
    }
  }, [client]);

  useEffect(() => {
    if (JSON.stringify(client) !== JSON.stringify(newClient)) {
      setDisable(false);
    } else {
      setDisable(true);
    }
  }, [newClient]);

  useEffect(() => {
    if (saveClicked) {
      dispatch(updateClient(newClient));
      setSaveClicked(false);
    }
  }, [saveClicked]);

  const onChangeHandler = (e: React.ChangeEvent<HTMLInputElement>, key: keyof ClientResponse) => {
    setNewClient((prevState) => ({ ...prevState, [key]: e.target.value.trim() }));
  };

  const getCurrentValue = (item: IRoleInfo | IStatusInfo): string => {
    if (item.name === 'blocked') {
      return getCurrentStatusName(newClient);
    }
    return '';
  };

  const selectHandler = (value: string) => {
    let changes = {};
    if (value in StatusEnum) {
      changes = { blocked: value === StatusEnum.Blocked };
    }

    setNewClient((prevState) => ({ ...prevState, ...changes }));
  };

  return (
    <SectionWrapper>
      Информация
      <PageFieldsWrapper padding={12}>
        {clientResponseModel.slice(1, 5).map((item) => (
          <TextField key={item.name} label={item.title}>
            <SimpleInput
              placeholder={'Не указано'}
              value={(newClient[item.name] ?? '').toString()}
              onChange={(e) => onChangeHandler(e, item.name)}
              disabled={item.name === 'phone'}
            />
          </TextField>
        ))}
        {[statusInfo].map((item) => (
          <TextField key={item.title} label={item.title}>
            <Select
              placeholder={''}
              options={item.options}
              eventHandler={selectHandler}
              value={getCurrentValue(item)}
            />
          </TextField>
        ))}
      </PageFieldsWrapper>
    </SectionWrapper>
  );
};

export default ClientFields;
