import React from 'react';
import styled from 'styled-components';
import { getTypography } from '../../theme/typography';
import { text_colors } from '../../theme/colors';
import { Link } from 'react-router-dom';
import { IBreadcrumbs } from '../../store/reducers/breadcrumb/CrumbSlice';
import { pxToRem } from '../../utils/Converting';

const Wrapper = styled.span`
  display: flex;
  align-items: flex-end;
  min-height: ${pxToRem(24)}rem;
`;

const Text = styled.span`
  ${getTypography('inputMedium')};
  color: ${text_colors.grey};
`;

interface IBreadcrumbsComponent {
  breadcrumbs: IBreadcrumbs[];
}

const Breadcrumbs: React.FC<IBreadcrumbsComponent> = ({ breadcrumbs }) => {
  return (
    <Wrapper>
      {breadcrumbs.map((item, idx) =>
        idx < breadcrumbs.length - 1 ? (
          <Link to={item.path} key={item.path}>
            <Text>{item.name + ' • '}</Text>
          </Link>
        ) : (
          <Text key={item.path}>{item.name}</Text>
        ),
      )}
    </Wrapper>
  );
};

export default Breadcrumbs;
