import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { pxToEm, pxToRem } from '../../utils/Converting';
import { IconBtn } from '../ui/buttons/IconBtn';
import ArrowLeft from '../../assets/icons/ArrowLeft';
import { colors, text_colors } from '../../theme/colors';
import Breadcrumbs from './Breadcrumbs';
import { useAppSelector } from '../../hooks/redux';
import { useNavigate } from 'react-router-dom';
import { getTypography } from '../../theme/typography';

const Wrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  align-self: stretch;
  padding: ${pxToRem(24)}rem;
`;

const Navigation = styled.div`
  display: flex;
  gap: ${pxToRem(36)}rem;
  align-self: stretch;
  flex: 1 1;
`;

const Title = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: flex-start;
  gap: ${pxToEm(6, 30)}em;

  ${getTypography('header0')};
  color: ${text_colors.dark_grey};
`;

const BtnsWrapper = styled.div`
  display: flex;
  gap: ${pxToRem(20)}rem;
`;

interface IHeading {
  children?: JSX.Element | JSX.Element[];
}

const Heading: React.FC<IHeading> = ({ children }) => {
  const navigate = useNavigate();
  const { title, breadcrumbs } = useAppSelector((state) => state.crumbReducer);
  const [isDisabled, setDisable] = useState(true);

  useEffect(() => {
    setDisable(breadcrumbs.length === 1);
  });

  return (
    <Wrapper>
      <Navigation>
        <IconBtn
          disabled={isDisabled}
          bg={colors.white}
          bg_hover={colors.icon_hover}
          onClick={() => navigate(-1)}
        >
          <ArrowLeft color={colors.brand} size={36} />
        </IconBtn>
        <Title>
          {title}
          <Breadcrumbs breadcrumbs={breadcrumbs} />
        </Title>
      </Navigation>
      <BtnsWrapper>{children}</BtnsWrapper>
    </Wrapper>
  );
};

export default Heading;
