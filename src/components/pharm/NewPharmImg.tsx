import React from 'react';
import styled from 'styled-components';
import { pxToRem } from '../../utils/Converting';
import { colors } from '../../theme/colors';
import { IconBtn } from '../ui/buttons/IconBtn';
import AddIcon from '../../assets/icons/AddIcon';

const BtnWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: ${pxToRem(16)}rem;
  gap: ${pxToRem(24)}rem;

  width: ${pxToRem(332)}rem;
  align-self: stretch;

  background: ${colors.input_hover};
  border-radius: 12px;
`;

interface INewPharmImg {
  length: number;
  addImg: (i: number) => void;
}

const NewPharmImg: React.FC<INewPharmImg> = ({ addImg, length }) => {
  return (
    <BtnWrapper>
      <IconBtn bg={colors.white} bg_hover={colors.icon_hover} onClick={() => addImg(length)}>
        <AddIcon />
      </IconBtn>
      Добавить новое изображение
    </BtnWrapper>
  );
};

export default NewPharmImg;
