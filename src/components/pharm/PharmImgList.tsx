import React from 'react';
import styled from 'styled-components';
import { pxToRem } from '../../utils/Converting';
import PharmImgCard from './PharmImgCard';
import NewPharmImg from './NewPharmImg';

const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-start;

  gap: ${pxToRem(32)}rem;

  flex-wrap: wrap;
`;

interface IPharmImgList {
  linkList: string[];
  addImg: (i: number, v?: string) => void;
  deleteImg: (i: number) => void;
}

const PharmImgList: React.FC<IPharmImgList> = ({ linkList, addImg, deleteImg }) => {
  return (
    <Wrapper>
      {linkList.map((item, idx) => (
        <PharmImgCard key={idx} link={item} deleteImg={deleteImg} addImg={addImg} idx={idx} />
      ))}
      <NewPharmImg addImg={addImg} length={linkList.length} />
    </Wrapper>
  );
};

export default PharmImgList;
