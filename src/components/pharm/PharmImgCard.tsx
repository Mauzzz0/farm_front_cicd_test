import React from 'react';
import styled from 'styled-components';
import { pxToRem } from '../../utils/Converting';
import { colors } from '../../theme/colors';
import TextField from '../ui/inputs/TextField';
import { SimpleInput } from '../ui/inputs/SimpleInput';
import { IconBtn } from '../ui/buttons/IconBtn';
import CloseIcon from '../../assets/icons/CloseIcon';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  padding: ${pxToRem(16)}rem;
  gap: ${pxToRem(32)}rem;
  box-sizing: border-box;

  border: 2px solid ${colors.grey};
  border-radius: 12px;
`;

const DeleteModal = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  padding: 0 10px;
  border-radius: 12px;

  background: rgba(105, 105, 105, 0.5);
  backdrop-filter: blur(2px);

  visibility: hidden;
`;

const ImgWrapper = styled.div`
  position: relative;
  width: ${pxToRem(300)}rem;
  height: ${pxToRem(200)}rem;
  border-radius: 12px;
  flex-wrap: wrap;

  &:hover ${DeleteModal} {
    visibility: visible;
  }
`;

const Img = styled.img`
  width: ${pxToRem(300)}rem;
  height: ${pxToRem(200)}rem;
  border-radius: 12px;
`;

interface IPharmImgCard {
  link: string;
  deleteImg: (i: number) => void;
  addImg: (i: number, v: string) => void;
  idx: number;
}

const PharmImgCard: React.FC<IPharmImgCard> = ({ link, deleteImg, addImg, idx }) => {
  return (
    <Wrapper>
      <ImgWrapper>
        <Img src={link} />
        <DeleteModal>
          <IconBtn bg={colors.grey} onClick={() => deleteImg(idx)}>
            <CloseIcon />
          </IconBtn>
        </DeleteModal>
      </ImgWrapper>
      <TextField label={'Ссылка на изображение'}>
        <SimpleInput
          placeholder={'Введите ссылку'}
          value={link}
          onChange={(e) => addImg(idx, e.target.value)}
        />
      </TextField>
    </Wrapper>
  );
};

export default PharmImgCard;
