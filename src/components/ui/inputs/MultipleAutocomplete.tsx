import React, { useState } from 'react';
import styled from 'styled-components';
import { pxToEm } from '../../../utils/Converting';
import { colors, text_colors } from '../../../theme/colors';
import { getTypography, textTypes } from '../../../theme/typography';
import ArrowDown from '../../../assets/icons/ArrowDown';
import InputTag from '../tags/InputTag';

const InputWrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  padding: 0 ${pxToEm(12, 18)}em 0 ${pxToEm(12, 18)}em;
  border: 2px solid ${colors.grey};
  border-radius: 12px;
  box-sizing: border-box;

  gap: ${pxToEm(4, textTypes.textRegular.font_size)}em;

  min-height: ${pxToEm(48, 18)}em;
  align-self: stretch;

  ${getTypography('inputMedium')};
  color: ${text_colors.dark_grey};

  &:hover {
    background: ${colors.input_hover};
  }
  &:active {
    border: 2px solid ${colors.dark_grey};
  }
  &::placeholder {
    color: ${text_colors.grey};
  }
  &:disabled {
    border: 2px dashed ${colors.grey};
    background: transparent;
  }
`;

const AutocompleteWrapper = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;

  align-self: stretch;

  max-height: ${pxToEm(48, 18)}em;
  ${getTypography('inputMedium')};
`;

interface IIconWrapper {
  rotation: number;
}

const IconWrapper = styled.div<IIconWrapper>`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  display: flex;
  justify-content: flex-end;
  align-items: center;
  padding: ${pxToEm(12, 18)}em;
  transform: rotate(${({ rotation }) => rotation}deg);
  transition: all 0.5s;
`;

interface IOptions {
  display: string;
}

const OptionsWrapper = styled.div<IOptions>`
  display: ${({ display }) => display};
  flex-direction: column;
  align-items: flex-start;
  align-self: stretch;
  flex-wrap: wrap;

  padding: ${pxToEm(16, 18)}em 0;
  box-shadow: 0 4px 30px rgba(0, 0, 0, 0.1);
  border-radius: 12px;

  background: ${colors.white};
  z-index: 2;
`;

const Option = styled.button`
  display: flex;
  padding: ${pxToEm(16, 18)}em ${pxToEm(12, 18)}em;
  align-self: stretch;
  background: transparent;
  border: none;
  z-index: 5;

  ${getTypography('inputMedium')};

  &:hover {
    background: ${colors.input_hover};
  }
  &:active {
    background: ${colors.grey};
  }
`;

interface ISelect {
  placeholder?: string;
  options: { name: string; title: string; bg: string }[];
  addOptions?: { name: string; title: string; bg: string }[];
  eventHandler?: (value: string) => void;
  onDeleteHandler: (s: string) => void;
  values?: { name: string | undefined; title: string; bg: string }[];
}

const MultipleAutocomplete: React.FC<ISelect> = ({
  // placeholder,
  options,
  addOptions,
  eventHandler,
  values,
  onDeleteHandler,
}) => {
  const [isHidden, setIsHidden] = useState(true);
  const [display, setDisplay] = useState('none');
  const [rotation, setRotation] = useState(0);

  if (addOptions) {
    options = options.concat(addOptions);
  }

  const changeVisibility = () => {
    if (isHidden) {
      setIsHidden(false);
      setDisplay('flex');
      setRotation(180);
    } else {
      setIsHidden(true);
      setDisplay('none');
      setRotation(0);
    }
  };

  // const makeHidden = (option: string, optionName: string) => {
  //   if (eventHandler) {
  //     eventHandler(optionName);
  //   }
  //   changeVisibility();
  // };

  const optionHandler = (optionName: string) => {
    if (eventHandler) {
      eventHandler(optionName);
    }
  };

  return (
    <AutocompleteWrapper>
      <InputWrapper onClick={() => changeVisibility()}>
        {values?.map((item) => (
          <InputTag
            key={item.name}
            name={item.name ?? ''}
            value={item.title}
            bg={item.bg}
            onDeleteHandler={onDeleteHandler}
          />
        ))}
      </InputWrapper>
      {/*<SimpleInput*/}
      {/*  placeholder={placeholder}*/}
      {/*  value={value}*/}
      {/*  onChange={() => value}*/}
      {/*  onClick={() => changeVisibility()}*/}
      {/*/>*/}
      <IconWrapper rotation={rotation} onClick={() => changeVisibility()}>
        <ArrowDown color={colors.dark_grey} />
      </IconWrapper>
      <OptionsWrapper hidden={isHidden} display={display}>
        {options.map((item) => (
          <Option key={item.name} onClick={() => optionHandler(item.name)}>
            {item.title}
          </Option>
        ))}
      </OptionsWrapper>
    </AutocompleteWrapper>
  );
};

export default MultipleAutocomplete;
