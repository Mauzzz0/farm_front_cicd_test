import React from 'react';
import styled from 'styled-components';
import LastRow from './LastRow';
import HeaderRow from './HeaderRow';
import TableRow from './TableRow';

const StyledTable = styled.div`
  box-sizing: border-box;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: flex-start;
  border: 2px solid #dddddd;
  border-radius: 10px;
  overflow: visible;
`;

interface ITable {
  header: { name: string; title: string }[];
  data: (string | JSX.Element)[][];
  minWidth?: number;
}

const Table: React.FC<ITable> = ({ header, data, minWidth }) => {
  return (
    <StyledTable>
      <HeaderRow data={header} minWidth={minWidth} />
      {data.map((row, idx) => (
        <TableRow key={idx} data={row} minWidth={minWidth} />
      ))}
      <LastRow />
    </StyledTable>
  );
};

export default Table;
