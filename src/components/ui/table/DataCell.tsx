import React from 'react';
import styled from 'styled-components';
import { pxToEm, pxToRem } from '../../../utils/Converting';
import { getTypography, textTypes } from '../../../theme/typography';
import { text_colors } from '../../../theme/colors';

const fontSize = textTypes.textRegular.font_size;

interface IStyledCell {
  minWidth?: number;
}

const StyledCell = styled.div<IStyledCell>`
  display: flex;
  flex-direction: row;
  align-items: center;
  flex-wrap: wrap;
  flex: 1 1;
  padding: 0 ${pxToEm(12, fontSize)}em;

  min-width: ${({ minWidth }) => (minWidth ? pxToRem(minWidth) : pxToRem(130))}rem;
  box-sizing: border-box;
  overflow: hidden;

  ${getTypography('textRegular')};
  color: ${text_colors.dark_grey};
`;

interface ITableCell {
  data: string | JSX.Element;
  minWidth?: number;
}

const DataCell: React.FC<ITableCell> = ({ data, minWidth }) => {
  return <StyledCell minWidth={minWidth}>{data}</StyledCell>;
};

export default DataCell;
