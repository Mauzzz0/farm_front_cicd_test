import React from 'react';
import styled from 'styled-components';
import { pxToEm, pxToRem } from '../../../utils/Converting';
import { getTypography, textTypes } from '../../../theme/typography';
import { text_colors } from '../../../theme/colors';

const fontSize = textTypes.textRegular.font_size;

interface IStyledCell {
  minWidth?: number;
}

const StyledCell = styled.div<IStyledCell>`
  display: flex;
  flex-direction: row;
  align-items: center;
  flex: 1 1;
  gap: ${pxToEm(10, fontSize)}em;

  min-width: ${({ minWidth }) => (minWidth ? pxToRem(minWidth) : pxToRem(130))}rem;

  ${getTypography('textBold')};
  color: ${text_colors.dark_grey};
`;

const Divider = styled.div`
  border: 1px solid ${text_colors.grey};
  align-self: stretch;
`;

interface ITableCell {
  data: string | JSX.Element;
  minWidth?: number;
}

const HeaderCell: React.FC<ITableCell> = ({ data, minWidth }) => {
  return (
    <StyledCell minWidth={minWidth}>
      <Divider />
      {data}
    </StyledCell>
  );
};

export default HeaderCell;
