import React from 'react';
import styled from 'styled-components';
import { colors } from '../../../theme/colors';
import { pxToRem } from '../../../utils/Converting';
import { IconBtn } from '../buttons/IconBtn';
import Pencil from '../../../assets/icons/Pencil';
import { useNavigate } from 'react-router-dom';
import DataCell from './DataCell';

const StyledRow = styled.div`
  display: flex;
  align-items: center;
  height: ${pxToRem(56)}rem;
  flex-grow: 1;
  align-self: stretch;
  border-bottom: 1px solid ${colors.grey};

  &:hover {
    background: ${colors.input_hover};
  }
`;

interface ITableRow {
  data: (string | JSX.Element)[];
  minWidth?: number;
}

const TableRow: React.FC<ITableRow> = ({ data, minWidth }) => {
  const navigate = useNavigate();
  return (
    <StyledRow>
      <IconBtn bg_hover={colors.icon_hover} onClick={() => navigate(`./${data[0]}`)}>
        <Pencil color={colors.dark_grey} />
      </IconBtn>
      {data.slice(1).map((item, idx) => (
        <DataCell key={idx} data={item} minWidth={minWidth} />
      ))}
    </StyledRow>
  );
};

export default TableRow;
