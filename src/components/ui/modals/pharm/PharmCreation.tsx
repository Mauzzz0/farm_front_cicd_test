import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { pxToRem } from '../../../../utils/Converting';
import { useAppDispatch } from '../../../../hooks/redux';
import { PharmResponse, pharmResponseModel } from '../../../../models/Pharmacy';
import TextField from '../../inputs/TextField';
import { SimpleInput } from '../../inputs/SimpleInput';
import { PrimaryBtn } from '../../buttons/PrimaryBtn';
import { postPharm } from '../../../../store/reducers/pharmacy/ActionCreators';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  gap: ${pxToRem(64)}rem;
`;

const Fields = styled.div`
  display: grid;
  grid-auto-flow: column;
  grid-template-rows: repeat(3, auto);
  flex-wrap: wrap;
  gap: ${pxToRem(32)}rem;
  align-self: stretch;
`;

interface IPharmCreation {
  setVisibility: React.Dispatch<React.SetStateAction<'hidden' | 'visible'>>;
}

const PharmCreation: React.FC<IPharmCreation> = ({ setVisibility }) => {
  const dispatch = useAppDispatch();
  const [isDisabled, setDisabled] = useState(true);
  const initialState: PharmResponse = {
    id: '',
    name: '',
    phone: '',
    works: '',
    map: '',
    lat: 0,
    lon: 0,
    icon: [],
  };
  const [pharm, setPharm] = useState<PharmResponse>(initialState);

  useEffect(() => {
    const isEmpty = Object.keys(initialState)
      .slice(0, 5)
      .reduce((prev, current) => {
        if (
          initialState[current as keyof PharmResponse] === pharm[current as keyof PharmResponse]
        ) {
          return true;
        }
        return prev;
      }, false);
    setDisabled(isEmpty);
  }, [pharm]);

  const onChangeHandler = (e: React.ChangeEvent<HTMLInputElement>, key: string) => {
    setPharm((prevState) => ({ ...prevState, [key]: e.target.value }));
  };

  const createPharm = async () => {
    await dispatch(postPharm(pharm));
    setVisibility('hidden');
    window.location.reload();
  };

  return (
    <Wrapper>
      <Fields>
        {pharmResponseModel.map((item) => (
          <TextField key={item.name} label={item.title}>
            <SimpleInput
              placeholder={'Не указано'}
              value={pharm ? pharm[item.name] : ''}
              onChange={(e) => onChangeHandler(e, item.name)}
            />
          </TextField>
        ))}
      </Fields>
      <PrimaryBtn size={'medium'} disabled={isDisabled} onClick={() => createPharm()}>
        Сохранить
      </PrimaryBtn>
    </Wrapper>
  );
};

export default PharmCreation;
