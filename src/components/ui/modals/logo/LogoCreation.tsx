import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { pxToRem } from '../../../../utils/Converting';
import { logoCreateModel, LogoInitResponse } from '../../../../models/Logo';
import TextField from '../../inputs/TextField';
import { SimpleInput } from '../../inputs/SimpleInput';
import { PrimaryBtn } from '../../buttons/PrimaryBtn';
import { useAppDispatch } from '../../../../hooks/redux';
import { postLogo } from '../../../../store/reducers/logo/ActionCreators';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  gap: ${pxToRem(64)}rem;
`;

const Fields = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  align-self: stretch;
  gap: ${pxToRem(32)}rem;
`;

interface ILogoCreation {
  setVisibility: React.Dispatch<React.SetStateAction<'hidden' | 'visible'>>;
}

const LogoCreation: React.FC<ILogoCreation> = ({ setVisibility }) => {
  const dispatch = useAppDispatch();
  const [isDisabled, setDisabled] = useState(true);
  const initialState: LogoInitResponse = {
    id: 0,
    url: '',
  };
  const [logo, setLogo] = useState<LogoInitResponse>(initialState);

  useEffect(() => {
    const isEmpty = Object.keys(initialState)
      .slice(0, 5)
      .reduce((prev, current) => {
        if (
          initialState[current as keyof LogoInitResponse] ===
          logo[current as keyof LogoInitResponse]
        ) {
          return true;
        }
        return prev;
      }, false);
    setDisabled(isEmpty);
  }, [logo]);

  const onChangeHandler = (e: React.ChangeEvent<HTMLInputElement>, key: string) => {
    setLogo((prevState) => ({ ...prevState, [key]: e.target.value }));
  };

  const createLogo = async () => {
    await dispatch(postLogo(logo));
    setVisibility('hidden');
    window.location.reload();
  };

  return (
    <Wrapper>
      <Fields>
        {logoCreateModel.map((item) => (
          <TextField key={item.name} label={item.title} width={item.name === 'url' ? '100%' : ''}>
            <SimpleInput
              placeholder={'Не указано'}
              value={logo[item.name]}
              onChange={(e) => onChangeHandler(e, item.name)}
            />
          </TextField>
        ))}
      </Fields>
      <PrimaryBtn size={'medium'} disabled={isDisabled} onClick={() => createLogo()}>
        Сохранить
      </PrimaryBtn>
    </Wrapper>
  );
};

export default LogoCreation;
