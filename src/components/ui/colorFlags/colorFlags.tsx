import styled from 'styled-components';
import React from 'react';
import { getTypography, textTypes } from '../../../theme/typography';
import { text_colors } from '../../../theme/colors';
import { pxToEm } from '../../../utils/Converting';

const fontSize = textTypes.textRegular.font_size;

interface IWrapper {
  width: number;
  bg: string;
}

const Wrapper = styled.div<IWrapper>`
  display: flex;
  flex-direction: row;
  align-items: center;
  gap: ${pxToEm(10, fontSize)}em;
  border-radius: 57px;
  padding: ${pxToEm(8, fontSize)}em ${pxToEm(16, fontSize)}em;
  box-sizing: border-box;

  min-width: ${({ width }) => pxToEm(width, fontSize)}em;
  background: ${({ bg }) => bg};

  ${getTypography('textRegular')};
  color: ${text_colors.dark_grey};
`;

interface IColorFlags {
  bg: string;
  icon: string;
  title: string;
  width: number;
}

const ColorFlags: React.FC<IColorFlags> = ({ bg, width, icon, title }) => {
  return (
    <Wrapper width={width} bg={bg}>
      <img src={icon} alt={''} />
      {title}
    </Wrapper>
  );
};

export default ColorFlags;
