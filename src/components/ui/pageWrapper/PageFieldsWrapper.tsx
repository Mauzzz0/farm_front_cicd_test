import styled from 'styled-components';
import { pxToRem } from '../../../utils/Converting';

interface IPageFieldsWrapper {
  padding?: number;
}

export const PageFieldsWrapper = styled.div<IPageFieldsWrapper>`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  align-items: flex-start;
  gap: ${pxToRem(48)}rem;
  padding: 0 ${({ padding }) => (padding ? pxToRem(padding) : 0)}rem;
  align-content: flex-start;
`;
