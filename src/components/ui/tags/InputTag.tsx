import React from 'react';
import styled from 'styled-components';
import { pxToEm } from '../../../utils/Converting';
import { getTypography, textTypes } from '../../../theme/typography';
import { text_colors } from '../../../theme/colors';
import CloseIcon from '../../../assets/icons/CloseIcon';

interface IWrapper {
  bg: string;
}

const Wrapper = styled.div<IWrapper>`
  box-sizing: border-box;
  display: flex;
  align-items: center;
  padding: ${pxToEm(6, textTypes.textRegular.font_size)}em;
  gap: ${pxToEm(6, textTypes.textRegular.font_size)}em;

  background: ${({ bg }) => bg};
  border-radius: 8px;

  ${getTypography('textRegular')};
  color: ${text_colors.dark_grey};
`;

const CloseButton = styled.button`
  border: none;
  padding: 0;
  border-radius: 8px;
  background: transparent;
  display: flex;
  flex: 1 1;
  opacity: 0.5;

  order: 20;

  &:hover {
    opacity: 1;
  }
`;

interface IInputTag {
  name: string;
  value: string;
  bg?: string;
  onDeleteHandler: (s: string) => void;
}

const InputTag: React.FC<IInputTag> = ({ name, value, bg, onDeleteHandler }) => {
  return (
    <Wrapper bg={bg ?? ''}>
      {value}
      <CloseButton onClick={() => onDeleteHandler(name)}>
        <CloseIcon size={16} />
      </CloseButton>
    </Wrapper>
  );
};

export default InputTag;
