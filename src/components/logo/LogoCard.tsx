import React from 'react';
import styled from 'styled-components';
import { pxToRem } from '../../utils/Converting';
import { colors, text_colors } from '../../theme/colors';
import { getTypography } from '../../theme/typography';
import { LogoResponse, logoResponseModel } from '../../models/Logo';
import { useNavigate } from 'react-router-dom';

const Wrapper = styled.button`
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  align-items: center;
  padding: ${pxToRem(16)}rem;

  width: ${pxToRem(268)}rem;
  height: ${pxToRem(312)}rem;

  background: ${colors.white};

  border: 2px solid ${colors.dark_grey};
  border-radius: 12px;

  &:hover {
    box-shadow: 2px 4px 8px rgba(23, 23, 23, 0.35);
  }

  &:active {
    opacity: 0.7;
  }
`;

const Img = styled.img`
  height: ${pxToRem(120)}rem;
  width: ${pxToRem(120)}rem;
`;

const TextTwoLines = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: flex-start;
  align-self: stretch;

  gap: ${pxToRem(8)}rem;

  color: ${text_colors.dark_grey};
`;

const TextLine = styled.span`
  display: flex;
  align-items: flex-start;
  align-self: stretch;
  gap: ${pxToRem(8)}rem;

  flex-wrap: wrap;

  width: 100%;
`;

const TextBold = styled.span`
  ${getTypography('textBold')};
`;

const TextRegular = styled.span`
  display: flex;
  text-align: start;
  align-self: stretch;
  flex: 1 1;
  overflow: hidden;
  ${getTypography('textRegular')}//flex-wrap: wrap;
`;

const LogoCard: React.FC<LogoResponse> = ({ ...props }) => {
  const navigate = useNavigate();

  return (
    <Wrapper onClick={() => navigate(`./${props.id}`)}>
      <TextTwoLines>
        {logoResponseModel.slice(0, 2).map((item) => (
          <TextLine key={item.name}>
            <TextBold children={`${item.title}:`} />
            <TextRegular children={props[item.name]} />
          </TextLine>
        ))}
      </TextTwoLines>
      <Img src={props.url} />
      <TextTwoLines>
        <TextBold children={`${logoResponseModel[2].title}:`} />
        <TextRegular children={props.url} />
      </TextTwoLines>
    </Wrapper>
  );
};

export default LogoCard;
