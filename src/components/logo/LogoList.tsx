import React from 'react';
import styled from 'styled-components';
import { pxToRem } from '../../utils/Converting';
import { LogoResponse } from '../../models/Logo';
import LogoCard from './LogoCard';

const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  flex-wrap: wrap;
  gap: ${pxToRem(28)}rem;
`;

interface ILogoList {
  logos: LogoResponse[];
}

const LogoList: React.FC<ILogoList> = ({ logos }) => {
  return (
    <Wrapper>
      {logos.map((item) => (
        <LogoCard key={item.id} id={item.id} groupName={item.groupName} url={item.url} />
      ))}
    </Wrapper>
  );
};

export default LogoList;
